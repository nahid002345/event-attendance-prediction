#!E:\Projects\uni\event-attendance-prediction\venv\Scripts\python.exe
# EASY-INSTALL-ENTRY-SCRIPT: 'magicgraph==0.0.1','console_scripts','magicgraph'
__requires__ = 'magicgraph==0.0.1'
import re
import sys
from pkg_resources import load_entry_point

if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw?|\.exe)?$', '', sys.argv[0])
    sys.exit(
        load_entry_point('magicgraph==0.0.1', 'console_scripts', 'magicgraph')()
    )
