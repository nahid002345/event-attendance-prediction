# -*- coding: utf-8 -*-
# Reference implementation of "PRUNE"
# Author: Yi-An Lai
# PRUNE: Preserving Proximity and Global Ranking for Network Embedding
# Yi-An Lai*, Chin-Chi Hsu*, Wen-Hao Chen, Ming-Han Feng, and Shou-De Lin
# Advances in Neural Information Processing Systems (NIPS), 2017

import numpy as np
import tensorflow as tf
from graph.embedding.static_graph_embedding import StaticGraphEmbedding
from time import time

class PRUNE(StaticGraphEmbedding):

    def __init__(self, *hyper_dict, **kwargs):
        ''' Initialize the node2vec2 class

        Args:
            lamb: lamb embedding
            n_emb: number of embedding
            nodeCount: node count
            dimension: dimensions
            learning_rate: learning rate
            epoch: epoch count
            gpu_fraction: gpu fraction
            batchsize: batch size
            print_every_epoch: print epoch
            save_checkpoints: checkpoint save option
        '''
        hyper_params = {
            'method_name': 'prune_rw'
        }
        hyper_params.update(kwargs)
        for key in hyper_params.keys():
            self.__setattr__('_%s' % key, hyper_params[key])
        for dictionary in hyper_dict:
            for key in dictionary:
                self.__setattr__('_%s' % key, dictionary[key])

    def forward_pass(self, featX, scope, n_emb, n_latent):
        """
        PRUNE's generative process for each node
        Generate ranking score and proximity representation
        """
        with tf.variable_scope(scope):
            # hidden layers dimension
            n_hidden_rank = 128
            n_hidden_prox = 128

            # global node ranking score
            W_hr = tf.get_variable("W_hidden_rank", [n_emb, n_hidden_rank])
            b_hr = tf.get_variable("b_hidden_rank", [n_hidden_rank])
            layer_rank = tf.nn.elu(tf.add(tf.matmul(featX, W_hr), b_hr))

            W_or = tf.get_variable("W_output_rank", [n_hidden_rank, 1])
            b_or = tf.get_variable("b_output_rank", [1])
            rank_pi = tf.nn.softplus(tf.add(tf.matmul(layer_rank, W_or), b_or))

            # proximity representation
            W_hp = tf.get_variable("W_hidden_prox", [n_emb, n_hidden_prox])
            b_hp = tf.get_variable("b_hidden_prox", [n_hidden_prox])
            layer_prox = tf.nn.elu(tf.add(tf.matmul(featX, W_hp), b_hp))

            W_op = tf.get_variable("W_output_prox", [n_hidden_prox, n_latent])
            b_op = tf.get_variable("b_output_prox", [n_latent])
            prox_rep = tf.nn.relu(tf.add(tf.matmul(layer_prox, W_op), b_op))

        return rank_pi, prox_rep

    def initialize_PRUNE(self, scope_name, n_emb, learning_rate, nodeCount, lamb):
        """
        Initialize PRUNE
        """
        # proximity representation dimension
        n_latent = 64
        tf.reset_default_graph()
        # indegree and outdegree
        outdeg = tf.placeholder("float", [None])
        indeg = tf.placeholder("float", [None])

        # indexes for head and tail nodes, PMI values
        node_heads = tf.placeholder(tf.int32, [None])
        node_tails = tf.placeholder(tf.int32, [None])
        pmis = tf.placeholder("float", [None, 1])

        with tf.variable_scope(scope_name , reuse= tf.AUTO_REUSE) as scope:
            # Create parameters
            initializer = tf.contrib.layers.xavier_initializer()
            tf.get_variable_scope().set_initializer(initializer)

            embeddings = tf.get_variable("embd", [nodeCount, n_emb])
            heads_emb = tf.gather(embeddings, node_heads)
            tails_emb = tf.gather(embeddings, node_tails)

            # W_shared
            W_init = np.identity(n_latent)
            W_init += abs(np.random.randn(n_latent, n_latent) / 1000.0)
            W_initializer = tf.constant_initializer(W_init)
            W_shared = tf.get_variable("W_shared", [n_latent, n_latent],
                                       initializer=W_initializer)
            W_shared_posi = tf.nn.relu(W_shared)

            # forward pass for head nodes
            heads_pi, heads_prox = self.forward_pass(heads_emb, scope, n_emb, n_latent)

            # Siamese neural network: reuse the NN defined
            tf.get_variable_scope().reuse_variables()

            # forward pass for tail nodes
            tails_pi, tails_prox = self.forward_pass(tails_emb, scope, n_emb, n_latent)

            zWz = heads_prox * tf.matmul(tails_prox, W_shared_posi)
            zWz = tf.reduce_sum(zWz, 1, keep_dims=True)

            # preserving proximity
            prox_loss = tf.reduce_mean((zWz - pmis) ** 2)

            # preserving global ranking
            r_loss = indeg * (tf.square(-tails_pi / indeg + heads_pi / outdeg))
            ranking_loss = tf.reduce_mean(r_loss)

            # define costs
            cost = prox_loss + lamb * ranking_loss

        optimizer = tf.train.AdamOptimizer(learning_rate).minimize(cost)

        # Initializing the variables
        init = tf.global_variables_initializer()

        inits = (init, optimizer, cost, heads_pi, heads_prox, tails_pi, tails_prox,
                 W_shared, node_heads, node_tails, indeg, outdeg, pmis, embeddings)
        return inits

    def train_PRUNE(self, init, optimizer, cost, node_heads, node_tails,
                    indeg, outdeg, pmis, embeddings, scope_name, epoch,
                    graph, PMI_values, batchsize, out_degrees, in_degrees,
                    gpu_fraction=0.20, print_every_epoch=3, save_cp=False):
        """
        PRUNE training process
        """
        # set 0 values to 1 to avoid divided by zero

        out_degrees[out_degrees == 0] = 1
        in_degrees[in_degrees == 0] = 1

        gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=gpu_fraction)
        with tf.variable_scope(scope_name, reuse=True) as scope:  # noqa

            tf_config = tf.ConfigProto(gpu_options=gpu_options)
            sess = tf.InteractiveSession(config=tf_config)
            with sess.as_default():

                # initialize variables
                sess.run(init)
                vars_to_train = tf.trainable_variables()
                saver = tf.train.Saver(vars_to_train)

                # Training epochs
                for n in range(epoch):
                    obj = 0.0
                    indexes = np.random.permutation(len(graph))

                    # Update parameters with mini-batch data
                    num_mini_batch = int(len(graph) / batchsize)
                    for i in range(num_mini_batch):
                        inds = indexes[i * batchsize: (i + 1) * batchsize]
                        edges = graph[inds]
                        pmi_vals = PMI_values[inds]

                        feed_dict = {
                            outdeg: out_degrees[edges[:, 0]],
                            indeg: in_degrees[edges[:, 1]],
                            pmis: pmi_vals,
                            node_heads: edges[:, 0].astype(np.int32),
                            node_tails: edges[:, 1].astype(np.int32)
                        }

                        _, c = sess.run([optimizer, cost], feed_dict=feed_dict)
                        obj += c

                    # Information printing
                    if print_every_epoch and ((n + 1) % print_every_epoch == 0):
                        print('\tepoch: %d; obj: %.7f' %
                              (n + 1, obj / num_mini_batch))

                    # TensorFlow checkpoints
                    if save_cp and (n + 1) % 10 == 0:
                        saver.save(sess, './checkpoints/PRUNE.ckpt',
                                   global_step=(n + 1))

                final_embeddings = sess.run(embeddings)

        return final_embeddings

    def train_networkx_PRUNE(self, init, optimizer, cost, node_heads, node_tails,
                             indeg, outdeg, pmis, embeddings, scope_name, epoch,
                             graph, PMI_values, batchsize,
                             gpu_fraction=0.20, print_every_epoch=3, save_cp=False):
        """
        PRUNE training process by networkx graph
        """
        # set 0 values to 1 to avoid divided by zero
        # out_degrees = list(graph.out_degree())
        # in_degrees = list(graph.in_degree())
        # out_degrees[out_degrees == 0] = 1
        # in_degrees[in_degrees == 0] = 1

        gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=gpu_fraction)
        with tf.variable_scope(scope_name, reuse=True) as scope:  # noqa

            tf_config = tf.ConfigProto(gpu_options=gpu_options)
            sess = tf.InteractiveSession(config=tf_config)
            with sess.as_default():

                # initialize variables
                sess.run(init)
                vars_to_train = tf.trainable_variables()
                saver = tf.train.Saver(vars_to_train)

                # Training epochs
                for n in range(epoch):
                    obj = 0.0
                    # changed code for networkx graph
                    indexes = np.array(list(graph.nodes))
                    np.random.shuffle(indexes)
                    # Update parameters with mini-batch data
                    num_mini_batch = int(len(indexes) / batchsize)
                    for i in range(num_mini_batch):
                        inds = indexes[i * batchsize: (i + 1) * batchsize]
                        edges = graph.edges(inds)
                        pmi_vals = np.asarray([PMI_values.get(x) for x in inds]).astype("float")
                        pmi_vals = np.vstack(np.expand_dims(pmi_vals, axis=1))

                        out_deg_edg_list = np.unique(np.asarray([x[0] for x in edges]))
                        in_deg_edg_list = np.unique(np.asarray([x[1] for x in edges]))
                        if len(out_deg_edg_list > len(in_deg_edg_list)):
                            out_deg_edg_list = np.asarray(list(set(out_deg_edg_list).intersection(in_deg_edg_list)))
                        else:
                            in_deg_edg_list = np.asarray(list(set(in_deg_edg_list).intersection(out_deg_edg_list)))


                        out_deg_edg_list = out_deg_edg_list[: batchsize]
                        in_deg_edg_list = in_deg_edg_list[: batchsize]
                        
                        # TODO: check in and out degree for edges . No considering for batch selected nodes
                        outdeg_list = np.asarray(list(graph.out_degree(inds))).astype("float")[:, 1]
                        indeg_list = np.asarray(list(graph.in_degree(inds))).astype("float")[:, 1]

                        outdeg_list = np.asarray([1 if i == 0 else i for i in outdeg_list])
                        indeg_list = np.asarray([1 if i == 0 else i for i in indeg_list])

                        node_head_list = np.asarray(list(graph.out_degree(out_deg_edg_list)))[:, 0]
                        node_tails_list = np.asarray(list(graph.in_degree(in_deg_edg_list)))[:, 0]
                        commn = list(set(node_head_list).intersection(node_tails_list))
                        feed_dict = {
                            outdeg: outdeg_list,
                            indeg: indeg_list,
                            pmis: pmi_vals,
                            node_heads: out_deg_edg_list.T.astype(np.int32),
                            node_tails: in_deg_edg_list.T.astype(np.int32)
                        }

                        _, c = sess.run([optimizer, cost], feed_dict=feed_dict)
                        obj += c

                    # Information printing
                    if print_every_epoch and ((n + 1) % print_every_epoch == 0):
                        print('\tepoch: %d; obj: %.7f' %
                              (n + 1, obj / num_mini_batch))

                    # TensorFlow checkpoints
                    if save_cp and (n + 1) % 10 == 0:
                        saver.save(sess, './checkpoints/PRUNE.ckpt',
                                   global_step=(n + 1))

                final_embeddings = sess.run(embeddings)

        return final_embeddings

    def compute_PMI(self, graph, nodeCount, in_degrees, out_degrees, alpha=5.0):
        """
        Compute pointwise mutual information (PMI) for each edge
        alpha: reprensents the number of effective negative samples
               for each positive sample
        """
        PMI_values = np.zeros((len(graph), 1))
        for ind in range(len(graph)):
            head, tail = graph[ind]
            pmi = len(graph) / alpha / out_degrees[head] / in_degrees[tail]
            PMI_values[ind, 0] = np.log(pmi)

        PMI_values[PMI_values < 0] = 0

        return PMI_values

    def compute_networkx_PMI(self, graph, alpha=5.0):
        """
        Compute pointwise mutual information (PMI) for each edge
        alpha: reprensents the number of effective negative samples
               for each positive sample
        """
        PMI_values = {}
        for edge in list(graph.edges()):
            # print(edge)
            head, tail = edge
            pmi = len(graph) / alpha / graph.out_degree(head) / graph.in_degree(tail)
            log_pmi = np.log(pmi)
            PMI_values[head] = log_pmi if log_pmi > 0 else 0

        return PMI_values

    def learn_embedding(self, graph=None, edge_f=None,
                        is_weighted=False, no_python=False):
        """
        Compute indegrees, outdegrees, PMI
        Initialize and train PRUNE for node embeddings
        """
        t1 = time()
        # remove self loop edges
        # graph.remove_edges_from(graph.selfloop_edges())
        # compute indegrees, outdegrees, PMI values
        # graph = np.loadtxt(self._inputgraph).astype(np.int32)
        graph = self.convert_sequential_node_number(graph)
        nodeCount = graph.max() + 1
        self._nodeCount = nodeCount


        scope_name = self._scope_name
        out_degrees = np.zeros(nodeCount)
        in_degrees = np.zeros(nodeCount)
        for node_i, node_j in graph:
            out_degrees[node_i] += 1
            in_degrees[node_j] += 1


        PMI_values = self.compute_PMI(graph, nodeCount, in_degrees, out_degrees)

        # initialize PRUNE
        inits = self.initialize_PRUNE(self._scope_name, self._dimension, self._learning_rate,
                                      self._nodeCount, self._lamb)

        (init, optimizer, cost, heads_pi, heads_prox, tails_pi, tails_prox,
         W_shared, node_heads, node_tails, indeg, outdeg, pmis, embeddings) = inits

        # train PRUNE
        self._embeddings = self.train_PRUNE(init, optimizer, cost, node_heads,
                                               node_tails, indeg, outdeg, pmis, embeddings,
                                               self._scope_name, self._epoch, graph, PMI_values,
                                               self._batchsize,out_degrees, in_degrees,
                                               gpu_fraction=self._gpu_fraction,
                                               print_every_epoch=self._print_every_epoch,
                                               save_cp=self._save_checkpoints)
        t2 = time()
        return self._embeddings, (t2-t1)

    def run_PRUNE(self, lamb: object, graph: object, nodeCount: object, n_emb: object, learning_rate: object,
                  epoch: object,
                  gpu_fraction: object = 0.20, batchsize: object = 1024, print_every_epoch: object = 1,
                  scope_name: object = 'default', save_cp: object = False) -> object:
        """
        Compute indegrees, outdegrees, PMI
        Initialize and train PRUNE for node embeddings
        """

        # compute indegrees, outdegrees, PMI values
        out_degrees = np.zeros(nodeCount)
        in_degrees = np.zeros(nodeCount)
        for node_i, node_j in graph:
            out_degrees[node_i] += 1
            in_degrees[node_j] += 1

        PMI_values = compute_PMI(graph, self._nodeCount, in_degrees, out_degrees)

        # initialize PRUNE
        inits = initialize_PRUNE(scope_name, n_emb, learning_rate,
                                 nodeCount, lamb)

        (init, optimizer, cost, heads_pi, heads_prox, tails_pi, tails_prox,
         W_shared, node_heads, node_tails, indeg, outdeg, pmis, embeddings) = inits

        # train PRUNE
        embeddings = train_PRUNE(init, optimizer, cost, node_heads,
                                 node_tails, indeg, outdeg, pmis, embeddings,
                                 scope_name, epoch, graph, PMI_values,
                                 batchsize, out_degrees, in_degrees,
                                 gpu_fraction=gpu_fraction,
                                 print_every_epoch=print_every_epoch,
                                 save_cp=save_cp)

        return embeddings

    def get_embedding(self):
        return self._embeddings
    def convert_sequential_node_number(self, graph):
        nodes = list(graph.nodes())
        grph = []

        edge_iter= 0
        for edge in list(graph.edges()):
            if edge[0]:
                f_edge_node_indx =  nodes.index(edge[0])
                s_edge_node_indx = nodes.index(edge[1])

                grph.append([f_edge_node_indx, s_edge_node_indx])
                # graph[edge_iter][1] = s_edge_node_indx

            edge_iter +=1

        return np.asarray(grph).astype(np.int32)

