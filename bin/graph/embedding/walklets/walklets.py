import glob
import random
import numpy as np
import pandas as pd
import networkx as nx
from tqdm import tqdm
from gensim.models.word2vec import Word2Vec
from graph.embedding.walklets.helper import walk_transformer, create_graph
from graph.embedding.walklets.walkers import FirstOrderRandomWalker, SecondOrderRandomWalker
from graph.util import graph_util, plot_util
from graph.evaluation import visualize_embedding as viz
from graph.embedding.static_graph_embedding import StaticGraphEmbedding
from time import time
class WalkletMachine(StaticGraphEmbedding):
    """
    Walklet multi-scale graph factorization machine class.
    The graph is being parsed up, random walks are initiated, embeddings are fitted, concatenated and the multi-scale embedding is dumped to disk.
    """

    def __init__(self, graph=None, window_size=5, dimensions=16, min_count=1, workers=4, walk_type='first',
                 walk_length=80, walk_number=5, walk_p=1, walk_q=1, output=None):
        """
        Walklet machine constructor.
        :param args: Arguments object with the model hyperparameters. 
        """
        hyper_params = {
            'method_name': 'walklets'
        }
        if graph:
            self.graph = graph
        self.window_size = window_size
        self.dimensions = dimensions
        self.min_count = min_count
        self.workers = workers
        self.output = output

        self.walk_type = walk_type
        if self.walk_type == "first":
            self.walker = FirstOrderRandomWalker(self.graph, walk_length, walk_number)
        else:
            self.walker = SecondOrderRandomWalker(self.graph, False, walk_length, walk_number, walk_p, walk_q)
            self.walker.preprocess_transition_probs()
        self.walks = self.walker.do_walks()
        del self.walker
        # self.create_embedding()
        # self.save_csv_model()

    def walk_extracts(self, length):
        """
        Extracted walks with skip equal to the length.
        :param length: Length of the skip to be used.
        :return good_walks: The attenuated random walks.
        """
        good_walks = [walk_transformer(walk, length) for walk in self.walks]
        good_walks = [w for walks in good_walks for w in walks]
        return good_walks

    def calculate_embedding(self, model):
        """
        Extracting the embedding according to node order from the embedding model.
        :param model: A Word2Vec model after model fitting.
        :return embedding: A numpy array with the embedding sorted by node IDs.
        """
        embedding = []
        node_list = list(self.graph.nodes())
        for node in node_list:
            embedding.append(list(model[str(node)]))
        embedding = np.array(embedding)
        return embedding

    def create_embedding(self):
        """
        Creating a multi-scale embedding.
        """
        self.embedding = []

        for index in range(1, self.window_size + 1):
            print("\nOptimization round: " + str(index) + "/" + str(self.window_size) + ".")
            print("Creating documents.")
            clean_documents = self.walk_extracts(index)
            print("Fitting model.")
            model = Word2Vec(clean_documents,
                             size=self.dimensions,
                             window=1,
                             min_count=self.min_count,
                             sg=1,
                             workers=self.workers)

            new_embedding = self.calculate_embedding(model)
            self.embedding = self.embedding + [new_embedding]
        self.embedding = np.concatenate(self.embedding, axis=1)

    def save_csv_model(self):
        """
        Saving the embedding as a csv with sorted IDs.
        """
        print("\nModels are integrated to be multi scale.\nSaving to disk.")
        self.column_names = ["x_" + str(x) for x in range(self.embedding.shape[1])]
        self.embedding = pd.DataFrame(self.embedding, columns=self.column_names)
        self.embedding.to_csv(self.output, index=None)

    def learn_embedding(self, graph=None, edge_f=None,
                        is_weighted=False, no_python=False):
        t1 = time()
        if graph:
            self.graph = graph
        if not graph and not edge_f:
            raise Exception('graph/edge_f needed')
        if edge_f:
            graph = graph_util.loadGraphFromEdgeListTxt(edge_f)
            self.graph = graph
        graph_util.saveGraphToEdgeListTxtn2v(graph, 'tempGraph_walklets.graph')

        # for testing just run it once and generate emb file
        self.create_embedding()
        # model.wv.save_word2vec_format('tempGraph_walklets.emb')

        # self._X = graph_util.loadEmbedding('tempGraph_walklets.emb')
        t2 = time()
        return self.embedding, (t2 - t1)

    def get_embedding(self):
        return self.embedding

    def get_edge_weight(self, i, j):
        return np.dot(self._X[i, :], self._X[j, :])

    def save_embedding(self):
        # TODO: save embedding
        return None