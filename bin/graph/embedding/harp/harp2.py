disp_avlbl = True
import os
if 'DISPLAY' not in os.environ:
    disp_avlbl = False
import matplotlib.pyplot as plt

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import scipy.io as sio
import scipy.sparse as sp
import scipy.sparse.linalg as lg
from time import time

import sys
sys.path.append('./')
sys.path.append(os.path.realpath(__file__))

from graph.embedding.static_graph_embedding import StaticGraphEmbedding

import magicgraph
import logging
import os
import sys

import numpy as np

from argparse import ArgumentParser, FileType, ArgumentDefaultsHelpFormatter
from magicgraph import WeightedDiGraph, WeightedNode
from scipy.io import mmread, mmwrite, loadmat

from graph.embedding.harp.graphCoarsening import *
from pathlib import Path

class HARP(StaticGraphEmbedding):
    # important: a custom library integration named 'magic-graph'
    def __init__(self, *hyper_dict, **kwargs):
        ''' Initialize the HOPE class

        Args:
            format: dimension of the embedding
            input: higher order coefficient
            sfdp-path
            model
            matfile-variable-name
            number-walks
            output
            representation-size
            walk-length
            window-size
            workers
        '''
        hyper_params = {
            'method_name': 'harp_svd',
            'format': 'edgelist',
            'input': Path('../dataset/graph.edgelist'),
            'sfdp_path': str(Path('../bin/graph/embedding/harp/bin/sfdp_windows.exe')),
            'model': 'node2vec',
            'matfile_variable_name': 'network',
            'number_walks': 40,
            'output': Path('../dataset/harp_output.npy'),
            'representation_size': 128,
            'walk_length': 10,
            'window_size': 10,
            'workers': 1
            }
        hyper_params.update(kwargs)
        for key in hyper_params.keys():
            self.__setattr__('_%s' % key, hyper_params[key])
        for dictionary in hyper_dict:
            for key in dictionary:
                self.__setattr__('_%s' % key, dictionary[key])

    def get_method_name(self):
        return self._method_name

    def get_method_summary(self):
        return '%s_%d' % (self._method_name)

    def learn_embedding(self, graph=None, edge_f=None,
                        is_weighted=False, no_python=False):
        t1 = time()
        if self._format == 'mat':
            graph = magicgraph.load_matfile(self._input, variable_name=self._matfile_variable_name, undirected=True)
        elif self._format == 'adjlist':
            graph = magicgraph.load_adjacencylist(self._input, undirected=True)
        elif self._format == 'edgelist':
            graph = magicgraph.load_edgelist(self._input, undirected=False)
        else:
            raise Exception("Unknown file format: '%s'. Valid formats: 'mat', 'adjlist', and 'edgelist'."
                            % self._format)
        graph = DoubleWeightedDiGraph(graph)
        print('Number of nodes: {}'.format(graph.number_of_nodes()))
        print('Number of edges: {}'.format(graph.number_of_edges()))
        print('Underlying network embedding model: {}'.format(self._model))

        if self._model == 'deepwalk':
            self._X = skipgram_coarsening_disconnected(graph, scale=-1, iter_count=1,
                                                                           sfdp_path=self._sfdp_path,
                                                                           num_paths=self._number_walks,
                                                                           path_length=self._walk_length,
                                                                           representation_size=self._representation_size,
                                                                           window_size=self._window_size,
                                                                           lr_scheme='default', alpha=0.025,
                                                                           min_alpha=0.001, sg=1, hs=1,
                                                                           coarsening_scheme=2, sample=0.1)
        elif self._model == 'node2vec':
            self._X = skipgram_coarsening_disconnected(graph, scale=-1, iter_count=1,
                                                                           sfdp_path=self._sfdp_path,
                                                                           num_paths=self._number_walks,
                                                                           path_length=self._walk_length,
                                                                           representation_size=self._representation_size,
                                                                           window_size=self._window_size,
                                                                           lr_scheme='default', alpha=0.025,
                                                                           min_alpha=0.001, sg=1, hs=0,
                                                                           coarsening_scheme=2, sample=0.1)
        elif self._model == 'line':
            self._X = skipgram_coarsening_disconnected(graph, scale=1, iter_count=50,
                                                                           sfdp_path=self._sfdp_path,
                                                                           representation_size=64, window_size=1,
                                                                           lr_scheme='default', alpha=0.025,
                                                                           min_alpha=0.001, sg=1, hs=0, sample=0.001)
        if self._output:
            np.save(self._output, self._X )
        t2 = time()

        return self._X, (t2 - t1)

    def get_embedding(self):
        return self._X


