from .static_graph_embedding import StaticGraphEmbedding


class Walklet2(StaticGraphEmbedding):
    def __init__(self, *hyper_dict, **kwargs):
        ''' Initialize the node2vec2 class

        Args:
            d: dimension of the embedding
            max_iter: max iterations
            walk_len: length of random walk
            num_walks: number of random walks
            con_size: context size
            ret_p: return weight
            inout_p: inout weight
        '''
        hyper_params = {
            'method_name': 'walklet_rw'
        }
        hyper_params.update(kwargs)
        for key in hyper_params.keys():
            self.__setattr__('_%s' % key, hyper_params[key])
        for dictionary in hyper_dict:
            for key in dictionary:
                self.__setattr__('_%s' % key, dictionary[key])

    def get_method_name(self):
        return self._method_name

    def get_method_summary(self):
        return '%s_%d' % (self._method_name, self._d)

    def learn_embedding(self, graph=None, edge_f=None,
                        is_weighted=False, no_python=False):
        args = ["walklet"]
        if not graph and not edge_f:
            raise Exception('graph/edge_f needed')
        if edge_f:
            graph = graph_util.loadGraphFromEdgeListTxt(edge_f)
        graph_util.saveGraphToEdgeListTxtn2v(graph, 'tempGraph_walklet.graph')

        # # for testing just run it once and generate emb file
        # node2vec_model = Node2Vec(graph=graph, dimensions=self._d, walk_length=self._walk_len,
        #                           num_walks=self._num_walks)
        # model = node2vec_model.fit(window=10, min_count=1, batch_words=4)
        # model.wv.save_word2vec_format('tempGraph.emb')

        t1 = time()
        self._X = graph_util.loadEmbedding('tempGraph.emb')
        t2 = time()
        return self._X, (t2 - t1)

    def get_embedding(self):
        return self._X

    def get_edge_weight(self, i, j):
        return np.dot(self._X[i, :], self._X[j, :])


