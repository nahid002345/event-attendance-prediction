import matplotlib.pyplot as plt
plt.switch_backend('agg')
import networkx as nx
from sklearn.manifold import TSNE
from sklearn.decomposition import PCA
import sys

sys.path.insert(0, './')
from graph.util import plot_util


class VisualEmbedding():
    def __init__(self, file_name, X, node_colors=None, di_graph=None, dim_reduce="pca",node_labels=None, fig_labels=None):
        self.file_name = file_name
        self.X = X
        self.node_colors = node_colors
        self.di_graph = di_graph
        self.dim_reduce = dim_reduce
        self.labels = node_labels
        self.fig_labels = fig_labels

    def plot_embedding2D(self, node_pos, node_colors=None, di_graph=None):
        # init plot
        plt.figure(figsize=(15,10))
        figure_label ="".join(str(self.fig_labels).split(',')[:10])
        plt.title(figure_label)
        node_num, embedding_dimension = node_pos.shape
        if (embedding_dimension > 2):
            if (self.dim_reduce == "pca"):
                print("Embedding dimension greater than 2, use PCA to reduce it to 2")
                model = PCA(n_components=2)
                node_pos = model.fit_transform(node_pos)
            else:
                print("Embedding dimension greater than 2, use tSNE to reduce it to 2")
                model = TSNE(n_components=2)
                node_pos = model.fit_transform(node_pos)

        if self.di_graph is None:
            # plot using plt scatter
            plt.scatter(node_pos[:, 0], node_pos[:, 1], c=node_colors)
        else:
            # get node list
            node_list = sorted(list(self.di_graph.nodes))

            # plot using networkx with edge structure
            pos = {}
            for i in range(node_num):
                pos[node_list[i]] = node_pos[i, :]
            if node_colors is not None:
                nx.draw_networkx_nodes(di_graph, pos,
                                       node_color=self.node_colors,
                                       width=0.1, node_size=100,
                                       arrows=False, alpha=0.8,
                                       font_size=5, labels=self.labels)
            else:
                nx.draw_networkx(self.di_graph, pos, node_color=self.node_colors,
                                 width=0.1, node_size=300, arrows=False,
                                 alpha=0.8, font_size=12, labels=self.labels)

    def expVis(self, node_labels=None):
        print('\tGraph Visualization:')
        if node_labels:
            node_colors = plot_util.get_node_color(node_labels)
        else:
            node_colors = None
        self.plot_embedding2D(self.X, node_colors=node_colors,
                         di_graph=self.di_graph)
        plt.savefig(str(self.file_name)+'.png', dpi=300,
                    format='png', bbox_inches='tight')
        plt.figure()
