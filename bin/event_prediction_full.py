from data_import import data_csv_import_array, column_from_matrix, is_file_exists, data_export_pandas_pickle, \
    data_import_pickle_panda
from util.text_processing import text_processed
# from util.evaluation import f1_score
import pandas as pd
import random
from sklearn import model_selection, preprocessing
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from scipy.sparse import csr_matrix, hstack
# from util.FNN import FNN
# import time
# import torch
# import pandas as pd
import numpy as np
# from collections import Counter
# from sklearn.datasets import fetch_20newsgroups
# from torch.autograd import Variable
# import torch.nn as nn
import torch.nn.functional as F
from sklearn.pipeline import Pipeline, FeatureUnion, make_pipeline
from NetworkEmbeddingFeature import NetworkEmbeddingFeature
from PreprocessTweetsExtractor import PreprocessTweetsExtractor
from DFFeatureExtractor import DFFeatureExtractor
from sklearn.svm import SVC
from sklearn.model_selection import cross_val_score, cross_validate, train_test_split, cross_val_predict, KFold
import joblib as jl
import pickle

def merge_duplicate_user_tweets(row):
    print(row.shape, ' ', row['user'])
    return row


def merge_tweet_label(row):
    value = 0
    row = row.tolist()
    row_count_one = row.count('1')
    row_count_zero = row.count('0')
    if row_count_one >= row_count_zero:
        value = 1
    else:
        value = 0
    return value;


def get_event_tweets_matrix():
    DATASET_NAME = "anthrocon_tweets"
    train_df = pd.DataFrame()

    # save panda dataframe to a pickle file so that load it easily and escape text processing part
    if not is_file_exists(DATASET_NAME + ".pkl"):
        data = data_csv_import_array(DATASET_NAME + ".csv")
        user_data = column_from_matrix(data, 2)
        text_data = column_from_matrix(data, 14)
        label_data = column_from_matrix(data, 16)
        sentiment_data = np.array(column_from_matrix(data, 13))
        train_df['user'] = user_data
        processed_text = text_processed(text_data)
        train_df['text'] = processed_text
        train_df['label'] = label_data
        train_df['sentiment'] = sentiment_data
        data_export_pandas_pickle(train_df, DATASET_NAME + ".pkl")
    else:
        train_df = data_import_pickle_panda(DATASET_NAME + ".pkl")

    ## combine duplicate user_node into a single row
    train_df = train_df.groupby('user').agg({
        'text': lambda x: ', '.join(x),
        'label': merge_tweet_label,
        'sentiment': lambda x: x.astype(np.float).mean()
    })
    train_x, valid_x, train_y, valid_y = model_selection.train_test_split(train_df['text'], train_df['label'])
    # label encode the target variable
    encoder = preprocessing.LabelEncoder()
    train_y = encoder.fit_transform(train_y)
    valid_y = encoder.fit_transform(valid_y)
    y_label_mat = encoder.fit_transform(train_df['label'])
    # feature extraction to matrix
    tfidf_vect_ngram = TfidfVectorizer(analyzer='word', token_pattern=r'\w{1,}', ngram_range=(2, 3),
                                       stop_words='english')
    tfidf_vect_ngram.fit(train_df['text'])
    csr_matr = tfidf_vect_ngram.transform(train_df['text'])
    train_df['tf_idf'] = list(csr_matr.toarray())
    dense_matr = csr_matr.todense()
    shpe = dense_matr.shape
    x_text_mat = tfidf_vect_ngram.transform(train_df['text']).todense()
    nb_shape = x_text_mat.shape
    list_len = len(train_df['sentiment'])
    # converting 1D sentiment data to 2D array for getting same dimension like textual sparse matrix
    sent_mat = np.reshape(train_df['sentiment'].values.astype(float), (len(train_df['sentiment']), -1))
    sent_shape = train_df['sentiment'].shape

    x_text_sentiment_mat = np.concatenate((x_text_mat, sent_mat), axis=1)
    n_shape = x_text_sentiment_mat.shape
    print("matrix shape: ", n_shape)

    train_df['text_sentiment'] = list(x_text_sentiment_mat)
    # TODO: planning to custome combine 2 features
    # TODO asdfassf asdfas

    # train_df['value'] = train_df['tf_idf'].apply(map(lambda row: item.tolist() + train_df['sentiment'] for item in row))
    # print(np.nonzero(train_df['tf_idf'][0]))
    # train_df['text_vector'] = x_text_sentiment_mat

    return x_text_sentiment_mat, train_df


def text_pipeline(file_name="anthrocon_tweets"):
    DATASET_NAME = file_name
    train_df = pd.DataFrame()
    # save panda dataframe to a pickle file so that load it easily and escape text processing part
    if not is_file_exists(DATASET_NAME + ".pkl"):
        data = data_csv_import_array(DATASET_NAME + ".csv")
        user_data = column_from_matrix(data, 2)
        text_data = column_from_matrix(data, 14)
        label_data = column_from_matrix(data, 16)
        sentiment_data = np.array(column_from_matrix(data, 13))
        train_df['user'] = user_data
        # processed_text = text_processed(text_data)
        train_df['text'] = processed_text
        train_df['label'] = label_data
        train_df['sentiment'] = sentiment_data
        data_export_pandas_pickle(train_df, DATASET_NAME + ".pkl")
    else:
        train_df = data_import_pickle_panda(DATASET_NAME + ".pkl")

    ## combine duplicate user_node into a single row
    train_df = train_df.groupby('user').agg({
        'text': lambda x: ', '.join(x),
        'label': merge_tweet_label,
        'sentiment': lambda x: x.astype(np.float).mean()
    })

    feature_file = pickle.load(open('feature_union.p', 'rb'))

    # text = Pipeline([('process_tweets', PreprocessTweetsExtractor('text')),
    #                  ('vct', TfidfVectorizer(analyzer='word', token_pattern=r'\w{1,}', ngram_range=(2, 3),
    #                                          stop_words='english'))])
    #
    # # Create the pipeline for the other variables and add selection to choose features
    # dummies = Pipeline([('dummies_transformation',
    #                      DFFeatureExtractor(columns=['sentiment']))])
    #
    # net_embed = Pipeline([('net_embed', NetworkEmbeddingFeature(model='node2vec', file_name='anthrocon.edgelist', ))])
    #
    # # Merge pipelines using FeatureUnion
    # features = FeatureUnion(transformer_list=[('text', text),
    #                                           ('dummies', dummies),
    #                                           ('net_embed', net_embed)],
    #                         transformer_weights={'text': 1.0, 'dummies': 1.0, 'net_embed': 1.0})
    #
    # # net_embed = Pipeline([('net_embed', NetworkEmbeddingFeature())])
    #
    pipeline = Pipeline([('clf', SVC(C=10, kernel='linear', gamma=.001))])
    #
    # feature_union = features.fit_transform(train_df, np.asarray(list(train_df['label'])))
    # pickle.dump(feature_union, open('feature_union.p', 'wb'))

    # y = pipeline.fit_transform(train_df, train_df['label'])
    scoring = ['precision', 'recall', 'f1', 'accuracy','roc_auc']
    scores = cross_validate(pipeline,feature_file, np.asarray(list(train_df['label'])),scoring=scoring, cv=3)
    roc_auc= cross_val_score(pipeline,feature_file, np.asarray(list(train_df['label'])), cv=3, scoring='roc_auc').mean()
    print(roc_auc)
    # Evaluate
    print('-----------------------------------')
    for key, values in scores.items():
        print(key, ' mean ', values.mean())
        print(key, ' std ', values.std())



# def event_prediction_main():
#     # initiate
#     start_time = time.time()
#     print_result = []
#     # data import and text processing
#     MIN_NUM_EPOCH = 5
#     MAX_NUM_EPOCH = 20
#
#     MIN_BATCH_SIZE = 50
#     MAX_BATCH_SIZE = 150
#
#     DATASET_NAME = "vfest_groundtruth.csv.csv"
#     print_result.append(DATASET_NAME)
#
#     data = data_csv_import_array(DATASET_NAME)
#     label_data = column_from_matrix(data, 1)
#     text_data = column_from_matrix(data, 2)
#     processed_text = text_processed(text_data)
#
#     trainDF = pd.DataFrame()
#     trainDF['text'] = processed_text
#     trainDF['label'] = label_data
#
#     train_x, valid_x, train_y, valid_y = model_selection.train_test_split(trainDF['text'], trainDF['label'])
#
#     # label encode the target variable
#     encoder = preprocessing.LabelEncoder()
#     train_y = encoder.fit_transform(train_y)
#     valid_y = encoder.fit_transform(valid_y)
#     # feature extraction to matrix
#     tfidf_vect_ngram = TfidfVectorizer(analyzer='word', token_pattern=r'\w{1,}', ngram_range=(2, 3),
#                                        stop_words='english')
#     tfidf_vect_ngram.fit(trainDF['text'])
#     xtrain_tfidf_ngram = tfidf_vect_ngram.transform(train_x)
#     xvalid_tfidf_ngram = tfidf_vect_ngram.transform(valid_x)
#
#     # Parameters
#     words_vocab_num = len(tfidf_vect_ngram.vocabulary_)
#     print_result.append(words_vocab_num)
#
#     learning_rate = 0.01
#     num_epochs = random.randint(MIN_NUM_EPOCH, MAX_NUM_EPOCH)
#     batch_size = random.randint(MIN_BATCH_SIZE, MAX_BATCH_SIZE)
#     display_step = 1
#
#     print_result.append(learning_rate)
#     print_result.append(num_epochs)
#     print_result.append(batch_size)
#     # Feed-forward Network Parameters
#     num_classes = 2  # binary classification of text
#     hidden_size = int(
#         (
#                 words_vocab_num + num_classes) / 2)  # mean of input and output layer . 1st layer and 2nd layer number of features
#     input_size = words_vocab_num  # Words in vocab
#     print_result.append(hidden_size)
#     net = FNN(input_size, hidden_size, num_classes)
#
#     # Loss and Optimizer
#
#     criterion = nn.CrossEntropyLoss()
#     optimizer = torch.optim.Adam(net.parameters(), lr=learning_rate)
#
#     # Train the Model
#     for epoch in range(num_epochs):
#         total_batch = int(len(train_x.data) / batch_size)
#         # Loop over all batches
#         for i in range(total_batch):
#             articles = Variable(torch.from_numpy(xtrain_tfidf_ngram.todense().astype("float32")))
#             labels = Variable(torch.from_numpy(train_y.astype("float32")).type(torch.LongTensor))
#
#             # Forward + Backward + Optimize
#             optimizer.zero_grad()  # zero the gradient buffer
#             outputs = net(articles)
#             loss = criterion(outputs, labels)
#             loss.backward()
#             optimizer.step()
#
#             if (i + 1) % 4 == 0:
#                 print('Epoch [%d/%d], Step [%d/%d], Loss: %.4f'
#                       % (epoch + 1, num_epochs, i + 1, len(train_x.data) // batch_size, loss.item()))
#
#     # Test the Model & Evaluation
#     total_test_data = len(valid_x.data)
#     articles = Variable(torch.from_numpy(xvalid_tfidf_ngram.todense().astype("float32")))
#     labels = torch.from_numpy(valid_y.astype("float32")).type(torch.LongTensor)
#     outputs = net(articles)
#     _, predicted = torch.max(outputs.data, 1)
#     accuracy, precision, recall, f1_measure = f1_score(labels, predicted)
#
#     process_time = (time.time() - start_time)
#     # print result in csv file
#     print_result.append(float(accuracy.float()))
#     print_result.append(float(precision.float()))
#     print_result.append(float(recall.float()))
#     print_result.append(float(f1_measure.float()))
#     print_result.append(process_time)
#
#     df = pd.DataFrame(
#         columns=['dataset', 'vocab', 'learning_rate', 'epoch', 'batch', 'hidden', 'accuracy', 'precision', 'recall',
#                  'f1',
#                  'time'])
#     df.loc[0] = print_result
#
#     df.to_csv('results.csv', sep=',', header=False, index=True, mode='a')
