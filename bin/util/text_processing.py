import re
import string

import numpy.random as npr
from data_import import data_csv_import
from nltk.corpus import stopwords,wordnet
from nltk.stem import PorterStemmer, WordNetLemmatizer
from nltk.tokenize import word_tokenize
from collections import Counter
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
# def text_processed():
#     txt = data_csv_import('20180401024547.csv')
#     # txt = data_txt_import_array('test.txt')
#     # txt = strip_punctation(txt.lower())
#     txt = remove_numeric_digit(txt)
#     format_token = text_stop_words(txt)
#     lemmeted_token = token_lemmetizer(format_token)
#     stemmed_token = token_stemmer(lemmeted_token)
#     str = ' '.join(stemmed_token)
#     str_array = re.split(r'[.]', str)
#     # print(txt)
#     # print('filtered_sentence \n')
#     # print(format_token)
#     # stemming
#     # print('Stemmed token \n')
#     # print(stemmed_token)
#     # lemm
#     # print('Lemmeted token \n')
#     # print(lemmeted_token)
#     # print('Lemmeted string \n')
#     # print(str)
#     return str_array


def text_processed(data_array):
    str_array = []
    for data in data_array:
        txt = remove_numeric_digit(data)
        format_token = text_stop_words(txt)
        lemmeted_token = token_lemmetizer(format_token)
        stemmed_token = token_stemmer(lemmeted_token)
        str = ' '.join(stemmed_token)
        str_array.append(str)
    return str_array

def single_text_processed(data_string):
    txt =remove_numeric_digit(strip_links(data_string))
    format_token = text_stop_words(txt)
    lemmeted_token = token_lemmetizer(format_token)
    stemmed_token = token_stemmer(lemmeted_token)
    str = ' '.join(stemmed_token)
    return str

def text_token_processed(data_array):
    str_array = []
    for data in data_array:
        txt = strip_all_entities(remove_hyperlink_text(data))
        txt = remove_numeric_digit(txt)
        txt = strip_punctation(txt)
        format_token = text_stop_words(txt)
        lemmeted_token = token_lemmetizer(format_token)
        stemmed_token = token_stemmer(lemmeted_token)

        str_array.extend(get_english_word_from_list(stemmed_token))
        # str_array = remove_duplication_from_arry(str_array)
    return sorted(str_array)

def string_token_process(text_string):
    token_array = []
    txt = strip_all_entities(remove_hyperlink_text(text_string))
    txt = remove_numeric_digit(txt)
    txt = strip_punctation(txt)
    format_token = text_stop_words(txt)
    lemmeted_token = token_lemmetizer(format_token)
    stemmed_token = token_stemmer(lemmeted_token)

    token_array.extend(get_english_word_from_list(stemmed_token))
    # str_array = remove_duplication_from_arry(str_array)
    return sorted(token_array)


def strip_links(text):
    link_regex = re.compile('((https?):((//)|(\\\\))+([\w\d:#@%/;$()~_?\+-=\\\.&](#!)?)*)', re.DOTALL)
    links = re.findall(link_regex, text)
    for link in links:
        text = text.replace(link[0], ', ')
    return text

def strip_links(text,substitute_text=' '):
    link_regex = re.compile('((https?):((//)|(\\\\))+([\w\d:#@%/;$()~_?\+-=\\\.&](#!)?)*)', re.DOTALL)
    links = re.findall(link_regex, text)
    for link in links:
        text = text.replace(link[0], substitute_text)
    return text

def remove_urls (text,substitute_text = ''):
    text = re.sub(r'(http|http[s]?:\/\/ )?[^\s(["<,>]*\.[^\s[",><]*', substitute_text, text, flags=re.MULTILINE)
    return text

def strip_all_entities(text):
    entity_prefixes = ['@', '#']
    for separator in string.punctuation:
        if separator not in entity_prefixes:
            text = text.replace(separator, ' ')
    words = []
    for word in text.split():
        word = word.strip()
        if word:
            if word[0] not in entity_prefixes:
                words.append(word)
    return ' '.join(words)

def get_english_word_from_list(word_check):
    word_list = []
    for word in word_check:
        if is_english_word(word):
            word_list.append(word)
    return word_list


def is_english_word(word):
    if wordnet.synsets(word):
        return  True
    else:
        return  False


def remove_duplication_from_arry(data):
    return list(set(data))


def remove_hyperlink_text(hyperlink_text):
    return re.sub(r'http\S+', '', hyperlink_text)


def text_stop_words(unformat_text):
    stop_words = set(stopwords.words('english'))
    word_tokens = word_tokenize(unformat_text)
    # print('tokens \n')
    # print(word_tokens)

    format_token = []
    for w in word_tokens:
        if w not in stop_words:
            format_token.append(w)

    return format_token


def strip_punctation(text):
    translate_table = dict((ord(char), None) for char in string.punctuation)
    stripped_text = text.translate(translate_table)
    return stripped_text


def remove_numeric_digit(text):
    result = ''.join([i for i in text if not i.isdigit()])
    return result


def token_stemmer(token):
    stemmer = PorterStemmer()
    stemmed_token = []
    for w in token:
        stemmed_token.append(stemmer.stem(w))
    return stemmed_token


def token_lemmetizer(token):
    wordnet_lemmatizer = WordNetLemmatizer()
    lemmeted_token = []
    for w in token:
        lemmeted_token.append(wordnet_lemmatizer.lemmatize(w, pos='v'))
    return lemmeted_token


def split_string_2_data_array(data, train_split=0.8):
    # data = np.array(data)
    num_train = int(len(data) * train_split)
    npr.shuffle(data)

    return (data[:num_train], data[num_train:])

def convert_list_to_counter(list):
    counts = Counter(list)
    return counts

def common_item_in_two_list(list_a, list_b):
    a_set = set(list_a)
    b_set = set(list_b)
    if len(a_set.intersection(b_set)) > 0:
        return(True)
    return(False)

def tweet_ploarity_scoring(tweet_text):
    if tweet_text:
        analyzer = SentimentIntensityAnalyzer()
        score = analyzer.polarity_scores(tweet_text)
    return score

def is_write_to_file_checking_name_first(name_score, scrn_name_score, polarity):
    is_write = False
    name_score = name_score[0][1]
    scrn_name_score = scrn_name_score[0][1]
    is_full_name_max = True if name_score >= scrn_name_score else False
    max_name_score = name_score if is_full_name_max is True else scrn_name_score

    if max_name_score > NAME_MATCHING_CHECKED_VALUE:
        is_write = True
        polarity_pole, polarity_value = self.check_and_get_polarity_theshold(polarity)
    else:
        polarity_pole, polarity_value = self.get_polarity_theshold(polarity)
        if not polarity_pole:
            is_write = True

    return is_write, polarity_pole, polarity_value