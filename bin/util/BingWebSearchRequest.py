
import http.client, urllib.parse, json,csv,time

# **********************************************
# *** Update or verify the following values. ***
# **********************************************

# Replace the subscriptionKey string value with your valid subscription key.
# https://azure.microsoft.com/en-us/try/cognitive-services/my-apis/?api=bing-web-search-api
subscriptionKey = "0c1097e1338941beafd44beeed2adbc4"

# Verify the endpoint URI.  At this writing, only one endpoint is used for Bing
# search APIs.  In the future, regional endpoints may be available.
host = "api.cognitive.microsoft.com"
path = "/bing/v7.0/search"

term = "site:sched.com"

def BingWebSearch(search,page=0,count=50):
    "Performs a Bing Web search and returns the results."

    headers = {'Ocp-Apim-Subscription-Key': subscriptionKey}
    conn = http.client.HTTPSConnection(host)
    query = urllib.parse.quote(search)
    date_parameter='&&filters=ex1%3a"ez5_16801_17166"'
    parameter_query = "?q=" + query + "&&count=" + str(count) + "&&offset="+ str(page) + date_parameter
    conn.request("GET", path + parameter_query, headers=headers)
    response = conn.getresponse()
    headers = [k + ": " + v for (k, v) in response.getheaders()
                   if k.startswith("BingAPIs-") or k.startswith("X-MSEdge-")]
    return headers, response.read().decode("utf8")
    
def write_to_csv(json_data):
    f = csv.writer(open("bing_sched_event_link_2016.csv", "a"))
    # Write CSV Header, If you dont need that, remove this line
    f.writerow(["id", "name", "url", "dateLastCrawled", "language"])
    
    for item in json_data:
        f.writerow([item["id"],
                    item["name"],
                    item["url"],
                    item["dateLastCrawled"],
                    item["language"]])
    
    time.sleep(1.5)



if len(subscriptionKey) == 32:

    print('Searching the Web for: ', term)

    start_page=0
    total_parse_result= 200
    for page in range(start_page,total_parse_result):
        headers, result = BingWebSearch(search=term,page=page)
        print("\nRelevant HTTP Headers:\n")
        print("\n".join(headers))
        print("\nJSON Response:\n")
        json_result = json.loads(result)
        if 'webPages' not in json_result:
          print("request 404")
          print(json_result.keys())
          print(term + " : page "+ str(page))
          pass
          # raise ValueError("No target in given data")
        else:
          write_to_csv(json_result['webPages']['value'])
          print(term + " : page "+ str(page))
        
    # headers, result = BingWebSearch(search=term,page=)
    

else:

    print("Invalid Bing Search API subscription key!")
    print("Please paste yours into the source code.")
