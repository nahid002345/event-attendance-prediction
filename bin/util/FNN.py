import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F


class FNN(nn.Module):

    def __init__(self, input_size, hidden_size, num_classes):
        super(FNN, self).__init__()
        self.layer_1 = nn.Linear(input_size, hidden_size, bias=True)
        self.relu = nn.ReLU()
        self.output_layer = nn.Linear(hidden_size, num_classes, bias=True)

    def forward(self, x):
        out = self.layer_1(x)
        out = self.relu(out)
        out = self.output_layer(out)
        return torch.sigmoid(out)
