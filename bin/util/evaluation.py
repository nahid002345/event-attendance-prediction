import torch


def f1_score(y_true, y_pred, threshold=0.5):
    return fbeta_score(y_true, y_pred, 1, threshold)


def fbeta_score(y_true, y_pred, beta, threshold, eps=1e-9):
    correct = 0
    total = 0
    beta2 = beta ** 2
    total += y_true.size(0)
    correct += (y_pred == y_true).sum()
    accuracy = (100 * correct / total)

    y_pred = torch.ge(y_pred.float(), threshold).float()
    y_true = y_true.float()

    true_positive = (y_pred * y_true).sum(dim=0)
    precision = true_positive.div(y_pred.sum(dim=0).add(eps))
    recall = true_positive.div(y_true.sum(dim=0).add(eps))
    f_measure = torch.mean(
        (precision * recall).
            div(precision.mul(beta2) + recall + eps).
            mul(1 + beta2))
    return accuracy, precision, recall, f_measure
