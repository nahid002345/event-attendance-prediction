from yellowbrick.classifier import PRCurve
##########################################################################
## Special, Named Colors
##########################################################################

YB_KEY = '#111111'  # The yellowbrick key (black) color is very dark grey
LINE_COLOR = YB_KEY  # Colors for best fit lines, diagonals, etc.


class PRCustom(PRCurve):
    def __init__(self, *args, **kwargs):
        super(PRCustom, self).__init__(*args, **kwargs)

    def _draw_binary(self):
        """
        Draw the precision-recall curves in the binary case
        """
        self._draw_pr_curve(self.recall_, self.precision_, label=self.name + " - binary PR curve")
        self._draw_ap_score(self.score_)

    def _draw_ap_score(self, score, label=None):
        """
        Helper function to draw the AP score annotation
        """
        label = label or "Avg Precision={:0.2f}".format(score)
        if self.ap_score:
            self.ax.axhline(
                y=score, color=self.color, ls="--", label=label
            )