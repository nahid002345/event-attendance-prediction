from yellowbrick import ROCAUC

##########################################################################
## Special, Named Colors
##########################################################################

YB_KEY = '#111111'  # The yellowbrick key (black) color is very dark grey
LINE_COLOR = YB_KEY # Colors for best fit lines, diagonals, etc.

class ROCCustom(ROCAUC):
    def __init__(self, *args, **kwargs):
        super(ROCCustom, self).__init__(*args, **kwargs)

    def draw(self):
        """
        Renders ROC-AUC plot.
        Called internally by score, possibly more than once

        Returns
        -------
        ax : the axis with the plotted figure
        """
        colors = self.colors[0:len(self.classes_)]
        n_classes = len(colors)

        # If it's a binary decision, plot the single ROC curve
        if self._binary_decision == True:
            self.ax.plot(
                self.fpr[0], self.tpr[0],
                label='ROC for binary decision, AUC = {:0.2f}'.format(
                        self.roc_auc[0]
                )
            )

        # If per-class plotting is requested, plot ROC curves for each class
        if self.per_class:
            self.ax.plot(
                self.fpr[0], self.tpr[0], color=self.colors[0],
                label='{} ROC of class {}, AUC = {:0.2f}'.format(
                    self.name, self.classes_[0], self.roc_auc[0],
                )
            )
                

        # If requested, plot the ROC curve for the micro average
        if self.micro:
            self.ax.plot(
                self.fpr[MICRO], self.tpr[MICRO], linestyle="--",
                color= self.colors[len(self.classes_)-1],
                label='micro-average ROC curve, AUC = {:0.2f}'.format(
                    self.roc_auc["micro"],
                )
            )

        # If requested, plot the ROC curve for the macro average
        if self.macro:
            self.ax.plot(
                self.fpr[MACRO], self.tpr[MACRO], linestyle="--",
                color= self.colors[len(self.classes_)-1],
                label='macro-average ROC curve, AUC = {:0.2f}'.format(
                    self.roc_auc["macro"],
                )
            )

        # Plot the line of no discrimination to compare the curve to.
        self.ax.plot([0,1], [0,1], linestyle=':', c=LINE_COLOR)
        return self.ax