# This package will contain the spiders of your Scrapy project
#
# Please refer to the documentation for information on how to create and manage
# your spiders.

import scrapy
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.exporters import CsvItemExporter
from scrapy.linkextractors import LinkExtractor
from scrapsched.items import ScrapschedItem, ScrapTwitterItem, SchedTwitterItem, Tweet, User, ScrapMeetupItem, \
    SchedEventTweeterSEItem
from scrapy import Request
from urlparser import urlparser
import re
import requests
from tweepy import models

import twitter
from urllib.parse import urlparse, parse_qs
import chardet
import csv
import tweepy
from fuzzywuzzy import fuzz, process
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
import logging
from datetime import datetime
import time
import pandas as pd
import logging

logger = logging.getLogger(__name__)
url_list = []
CONSUMER_KEY = 'oMOgH9nBmJuvhowTzPij2pYIZ'
CONSUMER_SECRET = 'VFkZrUL5UpMLJ9hhImpYxHmuKKznCU9ngMZgdZBZnMvolZyPEd'
ACCESS_TOKEN = '38574750-2nF0nmmwZAI1BJuGvJOgacWYp7oMnzsJ7zhxQWh1F'
ACCESS_TOKEN_SECRET = 'LKnOh7v74LIdXkklF7KIPOThxBcLymP7bWzRm5teEMYJS'

NAME_MATCHING_CUTOFF_VALUE = 30

NAME_MATCHING_CHECKED_VALUE = 80

auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
api = tweepy.API(auth)

start_time = time.time()
system_performance = []


def write_system_performance_info():
    df = pd.DataFrame(
        columns=['domain', 'event_name', 'total_participant', 'total_tweets', 'total_enlisted_tweets',
                 'total_tweets_user', 'time'])
    df.loc[0] = system_performance
    df.to_csv('system_performance.csv', sep=',', header=False, index=True, mode='a')


def write_tweets_list(item_dict):
    event_name = item_dict['event_name']
    with open(event_name + '_tweet_list.csv', 'a',encoding='utf-8') as csvfile:
        fieldnames = item_dict.keys()
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writerow(item_dict)
    # df = pd.DataFrame.from_dict(item_dict, orient='index')
    #
    # df.to_csv(event_name + '_tweet_list.csv', sep=',', header=True, index=True, mode='a')


def write_list_to_file(filename, list):
    with open(filename, 'w', encoding="utf-8") as filehandle:
        filehandle.writelines("%s\n" % item for item in list)


def read_list_from_file(filename):
    try:
        with open(filename, 'r', encoding="utf-8") as filehandle:
            return filehandle.read()
    except FileNotFoundError:
        pass


def get_urls_from_csv():
    with open(r'G:\Projects\uni\event-attendance-prediction\bin\crawl\scrapsched\scrapsched\event_list.csv',
              'r') as csv_file:
        data = csv.reader(csv_file)
        next(data)
        scrapurls = []
        hashtag = []
        for row in data:
            scrapurls.append(row[1])
            hashtag.append(row[6])

        return scrapurls,hashtag


def get_evevnt_name_from_csv():
    with open(
            r'G:\Projects\uni\event-attendance-prediction\bin\crawl\scrapsched\scrapsched\sched_event_url_all_search_engine.csv',
            'r') as csv_file:
        data = csv.reader(csv_file)
        next(data)
        event_name_list = []
        for row in data:
            event_name_list.append(row)

        return event_name_list


class SchedSpider(CrawlSpider):
    name = 'sched'
    allowed_domains = ['sched.com']
    start_urls = ["https://sched.com/directory"]
    rules = (
        Rule(LinkExtractor(allow=r'https://[^.\s]+\.sched\.com\/$', deny=r'https://[^.\s]+\.sched\.com\/robots*'),
             callback='parse_event', follow=True),)

    # def start_requests(self):
    #     for url in self.start_urls:
    #         yield scrapy.Request(url=url, callback=self.parse, dont_filter=True)
    #
    # def parse(self, response):
    #     page = response.url.split("/")[-2]
    #     filename = 'quotes-%s.html' % page
    #     with open(filename, 'wb') as f:
    #         f.write(response.body)

    def parse_event(self, response):
        item = ScrapschedItem()
        twitter_link = response.xpath("//div[@class='tweet']/ul/p[@class='tweet_more']/a/@href").extract()
        print(response.url, ":", twitter_link)
        if twitter_link:
            item['href'] = response.url
            event_name = response.xpath("//div[@id='schedorg-header-official']/a/text()").extract()
            if not event_name:
                event_name = response.xpath("//div[@id='schedorg-header-official']/text()").extract()
            if not event_name:
                event_name = response.url.split('.')
            item['name'] = event_name[0].replace("https://", "").replace("http://", "").strip()
            start_date = response.xpath("//span[@class='dtstart']/span/@title").extract()
            end_date = response.xpath("//span[@class='dtend']/span/@title").extract()
            print(start_date, end_date)
            item['date'] = start_date + end_date

            print(item)

    def parse_twitter(self, response):
        item = ScrapTwitterItem()
        return item


class SchedEventSpider(CrawlSpider):
    name = 'events'
    allowed_domains = ['sched.com']
    start_urls = ["https://sched.com/directory"]
    rules = (
        Rule(LinkExtractor(allow=r'https://[^.\s]+\.sched\.com\/$', deny=r'https://[^.\s]+\.sched\.com\/robots*'),
             callback='parse_event', follow=True),)
    # Create an Api instance.
    api = twitter.Api(consumer_key=CONSUMER_KEY,
                      consumer_secret=CONSUMER_SECRET,
                      access_token_key=ACCESS_TOKEN,
                      access_token_secret=ACCESS_TOKEN_SECRET)

    def parse_event(self, response):
        item = ScrapschedItem()
        item['href'] = response.url
        event_name = response.xpath("//div[@id='schedorg-header-official']/a/text()").extract()
        if not event_name:
            event_name = response.xpath("//div[@id='schedorg-header-official']/text()").extract()
        if not event_name:
            event_name = response.url.split('.')
        item['name'] = event_name[0].replace("https://", "").replace("http://", "").strip()
        start_date = response.xpath("//span[@class='dtstart']/span/@title").extract()
        if not start_date:
            start_date = response.xpath("//a[@class='sched-container-anchor']/@id").extract()
        item['date'] = start_date
        twitter_link = response.xpath("//div[@class='tweet']/ul/p[@class='tweet_more']/a/@href").extract()
        item['twitter_link'] = twitter_link
        yield item


class SchedEventSpider(CrawlSpider):
    name = 'event_all'
    allowed_domains = ['sched.com']
    start_urls = ["https://sched.com/directory"]
    rules = (
        Rule(LinkExtractor(allow=r'https://[^.\s]+\.sched\.com\/*', deny=r'https://[^.\s]+\.sched\.com\/robots*'),
             callback='parse_url', follow=True),)

    def parse_url(self, response):
        response_url = response.url
        url = urlparser.urlparse(response_url)
        subdomain_name = url.hostname.split('.')[0]
        if subdomain_name not in url_list:
            url_list.append(subdomain_name)
            req_url = 'https://' + subdomain_name + '.sched.com/'
            print(subdomain_name)
            yield Request(req_url, callback=self.parse_event, priority=1)

    def parse_event(self, response):
        item = ScrapschedItem()
        event_name = response.xpath("//div[@id='schedorg-header-official']/a/text()").extract()
        if not event_name:
            event_name = response.xpath("//div[@id='schedorg-header-official']/text()").extract()
        if not event_name:
            event_name = response.url.split('.')
        item['name'] = event_name[0].replace("https://", "").replace("http://", "").strip()
        start_date = response.xpath("//span[@class='dtstart']/span/@title").extract()
        if not start_date:
            start_date = response.xpath("//a[@class='sched-container-anchor']/@id").extract()
        item['date'] = start_date
        twitter_link = response.xpath("//div[@class='tweet']/ul/p[@class='tweet_more']/a/@href").extract()
        item['twitter_link'] = twitter_link

        yield item


# get sched event links from twitter search option
# first get all event subdomain searched in twitter
# then search their attendances list
class TwitterSpider(scrapy.Spider):
    name = 'twitter'
    start_urls = ["https://twitter.com/search?l=&q=sched.com%20since:2014-01-01%20until:2018-11-01&src=typd", ]

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url=url, callback=self.parse_twitter, dont_filter=False)

    def parse_twitter(self, response):
        stream = response.xpath(
            "//div[@class='stream']/ol/li/div/div[@class='content']/div[contains(@class,'js-tweet-text-container')]/p/a/@href").extract()
        # stream = response.xpath("//span[contains(@class,'u-block TwitterCardsGrid-col--spacerTop SummaryCard-destination')]").extract()
        print(len(stream))

        urls = re.findall('https?://\S+', ' , '.join(stream))
        for url in urls:
            try:
                r = requests.head(url, allow_redirects=True)
                url = urlparser.urlparse(r.url)
                subdomain_name = url.hostname.split('.')[0]
                item = ScrapschedItem()
                item['name'] = subdomain_name
                if subdomain_name not in url_list:
                    item['repetition'] = 0
                    url_list.append(subdomain_name)
                    home_url = 'https://' + subdomain_name + '.sched.com'
                    attendees_url = home_url + '/directory/attendees'
                    print(attendees_url)

                    yield Request(url=attendees_url, callback=self.parse_sched_attendees, dont_filter=False,
                                  meta={'item': item})
            except UnicodeDecodeError:
                print("unicodedecode error:  " + str(url))
                pass

    #
    def parse_sched_event(self, response):
        item = response.meta['item']
        start_date = response.xpath("//span[@class='dtstart']/span/@title").extract()
        if not start_date:
            start_date = response.xpath("//a[@class='sched-container-anchor']/@id").extract()
        item['date'] = start_date

    def parse_sched_attendees(self, response):
        if response.status != 404:
            item = response.meta['item']
            subdomain_name = self.get_url_to_subdomain(response.url)
            item['href'] = 'https://' + subdomain_name + '.sched.com/'

            # extract first attendees list
            participents = self.parse_sched_attendance_list(response)
            item['participant_list'] = len(participents)

            # extract pagination information
            no_pages = response.xpath(
                "//div[@class='sched-paging']/a")
            page = 1
            item['current_page'] = page
            last_page_number = 0
            if len(no_pages) > 1:
                last_page_hyperlink = response.xpath("//div[@class='sched-paging']/a/text()").extract()[-1]
                if last_page_hyperlink:
                    last_page_number = int(last_page_hyperlink)
                    print(last_page_number)

            item['total_page'] = last_page_number
            is_last_page = False;

            while page < last_page_number:
                page = page + 1

                req_url = "https://" + subdomain_name + ".sched.com/directory/attendees/" + str(page)
                if page is last_page_number:
                    is_last_page = True
                yield Request(url=req_url, callback=self.parse_sched_pagination, dont_filter=False,
                              meta={'item': item, 'is_last': is_last_page, 'total_page': last_page_number,
                                    'current_page': (page)})
            print("total of " + item['name'] + " count:" + str(item['participant_list']))

    def parse_sched_pagination(self, response):
        if response.status != 404:
            item = response.meta['item']
            participant = self.parse_sched_attendance_list(response)
            item['participant_list'] = item['participant_list'] + len(participant)
            item['current_page'] = item['current_page'] + 1
            total_page = response.meta['total_page']
            if item['current_page'] == total_page:
                print(item)
                yield item

    def parse_sched_attendance_list(self, response):
        if response.status != 404:
            attendees = response.xpath(
                "//div[@class='sched-container-people']/div[@class='sched-person']/a/@title").extract()
            return attendees

    def get_url_to_subdomain(self, url):
        url_new = urlparser.urlparse(url)
        subdomain = url_new.hostname.split('.')[0]
        return subdomain


# get sched event list from search engine by a csv file
# first get all event subdomain searched in twitter
# then search their attendances list
class SchedEventSearchEngineSpider(scrapy.Spider):
    name = 'sched_event_list_SE'
    start_urls = get_evevnt_name_from_csv()

    def start_requests(self):
        for event_info in self.start_urls:
            event_name = event_info[0].strip()
            search_engine = event_info[1].strip()
            event_url = 'https://' + event_name + '.sched.com'
            yield Request(url=event_url, callback=self.parse_sched_event, dont_filter=False,
                          meta={'event_name': event_name, 'search_engine': search_engine})

    def parse_sched_event(self, response):
        if response.status != 404:
            item = SchedEventTweeterSEItem()
            item['name'] = response.meta['event_name']
            item['search_engine'] = response.meta['search_engine']

            start_date = response.xpath("//span[@class='dtstart']/span/@title").extract()
            if not start_date:
                start_date = response.xpath("//a[@class='sched-container-anchor']/@id").extract()
            item['date'] = start_date
            # extract twitter hashtag
            twitter_hashtag_url = response.xpath(
                "//div[@class='tweet']/ul/p[@class='tweet_more']/a/@href").extract_first()
            parse_url = urlparse(twitter_hashtag_url)
            url_param = parse_qs(parse_url.query, encoding='utf-8')
            if url_param:
                hashtag = url_param.get("q")
                print("hash# : " + str(hashtag))
                item['twitter_hashtag'] = ''.join(hashtag)
            subdomain_name = response.meta['event_name']
            url_attend = "https://" + subdomain_name + ".sched.com/directory/attendees"
            yield Request(url=url_attend, callback=self.parse_sched_event_attendees, dont_filter=False,
                          meta={'item': item})

    def parse_sched_event_attendees(self, response):
        if response.status != 404:
            item = response.meta['item']
            subdomain_name = item['name']

            # extract pagination information
            no_pages = response.xpath(
                "//div[@class='sched-paging']/a")
            print(len(no_pages))
            page = 0
            item['current_page'] = page
            last_page_number = 0
            if len(no_pages) >= 1:
                last_page_hyperlink = response.xpath("//div[@class='sched-paging']/a/text()").extract()[-1]
                if last_page_hyperlink:
                    last_page_number = int(last_page_hyperlink)
                    print(last_page_number)

            item['total_page'] = last_page_number
            while page < last_page_number:
                page = page + 1
                req_url = "https://" + subdomain_name + ".sched.com/directory/attendees/" + str(page)
                yield Request(url=req_url, callback=self.parse_sched_pagination, dont_filter=False,
                              meta={'item': item,
                                    'total_page': last_page_number})

    def parse_sched_pagination(self, response):
        if response.status != 404:
            item = response.meta['item']
            participant = self.parse_sched_attendance_list(response)

            if 'participant_list' in item.keys():
                previous_list = item['participant_list']
                item['participant_list'] = int(previous_list) + len(participant)
            else:
                item['participant_list'] = len(participant)

            item['current_page'] = item['current_page'] + 1
            total_page = response.meta['total_page']
            if item['current_page'] == total_page:
                print('total_participant : ' + str(item['participant_list']))
                search_param = item['name'] + ' OR ' + item['name'] + '.sched.com'
                hashtag = item.get('twitter_hashtag', None)
                if hashtag is not None:
                    search_param = search_param + ' OR ' + item['twitter_hashtag']
                twitter_search_url = "https://twitter.com/search?f=tweets&q=" + search_param + "&lang=en"
                yield Request(url=twitter_search_url, callback=self.parse_twitter_list, dont_filter=False,
                              meta={'item': item})

    def parse_sched_attendance_list(self, response):
        if response.status != 404:
            attendees = response.xpath(
                "//div[@class='sched-container-people']/div[@class='sched-person']/a/@title").extract()
            # check if the name is more than 3 character long
            attendees = [attendee_name for attendee_name in attendees if
                         len(re.sub('[^A-Za-z0-9]+', '', attendee_name)) > 3]
            return attendees

    def parse_twitter_list(self, response):
        item = response.meta['item']
        tweet_list_stream = response.xpath('//li[@data-item-type="tweet"]/div')
        print("tweet list: " + str(len(tweet_list_stream)))
        item['total_tweets'] = len(tweet_list_stream)
        yield item
        pass


class SchedTwitterSpider(scrapy.Spider):
    name = 'sched_twitter'
    start_urls,hastag = get_urls_from_csv()

    def get_url_to_subdomain(self, url):
        url_new = urlparser.urlparse(url)
        subdomain = url_new.hostname.split('.')[0]
        return subdomain

    def start_requests(self):
        item = 0
        for url in self.start_urls:
            item +=1
            url_home = "https://" + url + ".sched.com/"
            yield Request(url=url_home, callback=self.parse_sched_event, dont_filter=False,meta={'hashtag': self.hastag[item-1]})

    def parse_sched_event(self, response):
        item = SchedTwitterItem()
        hashtag = response.meta['hashtag']
        start_date = response.xpath("//span[@class='dtstart']/span/@title").extract()
        if not start_date:
            start_date = response.xpath("//a[@class='sched-container-anchor']/@id").extract()
        item['date'] = start_date
        # extract twitter hashtag
        if not hashtag:
            twitter_hashtag_url = response.xpath("//div[@class='tweet']/ul/p[@class='tweet_more']/a/@href").extract_first()
            parse_url = urlparse(twitter_hashtag_url)
            url_param = parse_qs(parse_url.query, encoding='utf-8')
            if url_param:
                hashtag = url_param.get("q")
                print("hash# : " + str(hashtag))
                item['twitter_hashtag'] = ''.join(hashtag)
        else:
            item['twitter_hashtag'] = hashtag
        subdomain_name = self.get_url_to_subdomain(response.url)
        url_attend = "https://" + subdomain_name + ".sched.com/directory/attendees"
        yield Request(url=url_attend, callback=self.parse_sched_event_attendees, dont_filter=False, meta={'item': item})

    def parse_sched_event_attendees(self, response):
        if response.status != 404:
            item = response.meta['item']
            subdomain_name = self.get_url_to_subdomain(response.url)
            item['name'] = subdomain_name

            participents_file = read_list_from_file(item['name'] + '_sched_attendees_list.txt')

            # extract first attendees list
            participents = self.parse_sched_attendance_list(response)
            item['participant_list'] = participents

            # extract pagination information
            no_pages = response.xpath(
                "//div[@class='sched-paging']/a")
            print(len(no_pages))
            page = 1
            item['current_page'] = page
            last_page_number = 0
            if len(no_pages) > 1:
                last_page_hyperlink = response.xpath("//div[@class='sched-paging']/a/text()").extract()[-1]
                if last_page_hyperlink:
                    last_page_number = int(last_page_hyperlink)
                    print(last_page_number)

            item['total_page'] = last_page_number
            if participents_file:
                participents = participents_file.split('\n')
                item['participant_list'] = participents
                search_param = item['name'] + ' OR ' + item['name'] + '.sched.com'
                hashtag = item.get('twitter_hashtag', None)
                if hashtag is not None:
                    search_param = search_param + ' OR ' + item['twitter_hashtag']
                twitter_search_url = "https://twitter.com/search?f=tweets&q=" + search_param + "&lang=en"
                yield Request(url=twitter_search_url, callback=self.twitter_scrape_search, dont_filter=False,
                              meta={'item': item, })
            else:
                while page < last_page_number:
                    page = page + 1

                    req_url = "https://" + subdomain_name + ".sched.com/directory/attendees/" + str(page)
                    yield Request(url=req_url, callback=self.parse_sched_pagination, dont_filter=False,
                                  meta={'item': item,
                                        'total_page': last_page_number})
            print("total of " + item['name'] + " count:" + str(item['participant_list']))

    def parse_sched_pagination(self, response):
        if response.status != 404:
            item = response.meta['item']
            participant = self.parse_sched_attendance_list(response)
            previous_list = item['participant_list']
            previous_list.extend(participant)
            item['participant_list'] = list(set(previous_list))
            item['current_page'] = item['current_page'] + 1
            total_page = response.meta['total_page']
            if item['current_page'] == total_page:
                logging.info('total_participant : ' + str(len(item['participant_list'])))
                write_list_to_file(item['name'] + '_sched_attendees_list.txt', item['participant_list'])

                search_param = item['name'] + ' OR ' + item['name'] + '.sched.com'
                hashtag = item.get('twitter_hashtag', None)
                if hashtag is not None:
                    search_param = search_param + ' OR ' + item['twitter_hashtag']
                # api_tweets = self.tweepy_api_request(search_param)
                # if len(api_tweets) < 1:
                twitter_search_url = "https://twitter.com/search?f=tweets&q=" + search_param + "&lang=en"
                yield Request(url=twitter_search_url, callback=self.twitter_scrape_search, dont_filter=False,
                              meta={'item': item, })
                # else:
                #     self.search_result_to_generic_tweets(api_tweets,item)

    def parse_sched_attendance_list(self, response):
        if response.status != 404:
            attendees = response.xpath(
                "//div[@class='sched-container-people']/div[@class='sched-person']/a/@title").extract()
            # check if the name is more than 3 character long
            attendees = [attendee_name for attendee_name in attendees if
                         len(re.sub('[^A-Za-z0-9]+', '', attendee_name)) > 3]
            return attendees

    def search_result_to_generic_tweets(self, searched_tweets, item):
        attendees_list = item['participant_list']
        tweet_list = []
        for search_tweet in searched_tweets:
            try:
                tweet = Tweet()
                tweet['event_name'] = item['name']
                tweet['ID'] = search_tweet.id
                ### get text content
                tweet['text'] = search_tweet.text
                # tweet['datetime'] = search_tweet.created_at
                tweet['user_id'] = search_tweet.author.id
                tweet['name'] = search_tweet.author.name
                tweet['screen_name'] = search_tweet.author.screen_name

                tweet['twitter_polarity'] = self.tweet_ploarity_scoring(tweet['text'])
                tweet['name_match_score'] = self.name_matching(attendees_list, tweet['user_name'], 1)
                tweet['scrn_name_match_score'] = self.name_matching(attendees_list, tweet['screen_name'], 1)
                yield tweet
                tweet_list.append(tweet)
            except:
                pass

        # self.save_tweet_list_to_file(tweet_list,item['name']+'.csv')

    def twitter_scrape_search(self, response):
        if response.status != 404:
            sched_item = response.meta['item']
            attendees_list = sched_item['participant_list']
            loop = 1
            tweet_write_to_file = []
            tweet_user_write_to_file = []
            tweet_list_stream = response.xpath('//li[@data-item-type="tweet"]/div')
            logging.info('number of total_tweets : ' + str(len(tweet_list_stream)))
            tweet_list = []
            tracked_user_id_list = []
            followers_list_file = open(sched_item['name'] + '_followers_edge_list.txt', 'w')
            for stream in tweet_list_stream:
                loop += 1
                try:
                    tweet = Tweet()
                    tweet['event_name'] = sched_item['name']
                    ID = stream.xpath('.//@data-tweet-id').extract()
                    if not ID:
                        continue
                    tweet['ID'] = ID[0]
                    ### get text content
                    tweet_post = ' '.join(
                        stream.xpath('.//div[@class="js-tweet-text-container"]/p//text()').extract()).replace(' # ',
                                                                                                              '#').replace(
                        ' @ ', '@')
                    tweet_post = '"' + re.sub('\s+', ' ', tweet_post) + '"'
                    tweet['text'] = tweet_post.strip('\t\n\r')
                    if tweet['text'] == '':
                        # If there is not text, we ignore the tweet
                        continue
                    # tweet['datetime'] = datetime.fromtimestamp(int(
                    #     stream.xpath(
                    #         './/div[@class="stream-item-header"]/small[@class="time"]/a/span/@data-time').extract()[
                    #         0])).strftime('%Y-%m-%d %H:%M:%S')
                    tweet['user_id'] = stream.xpath('.//@data-user-id').extract()[0]
                    tweet['name'] = stream.xpath('.//@data-name').extract()[0]
                    tweet['screen_name'] = stream.xpath('.//@data-screen-name').extract()[0]
                    tweet['twitter_polarity'] = self.tweet_ploarity_scoring(tweet['text'])
                    tweet['name_match_score'] = self.name_matching(attendees_list, tweet['name'], 1)
                    tweet['scrn_name_match_score'] = self.name_matching(attendees_list, tweet['screen_name'], 1)
                    is_write, is_positive, polarity_value = self.is_write_to_file_checking_name_first(
                        tweet['name_match_score'],
                        tweet['scrn_name_match_score'],
                        tweet['twitter_polarity'])
                    tweet['post_label'] = '1' if is_positive is True else '0'
                    tweet['polarity_score'] = polarity_value

                    logging.info('tweet stream loop count : ' + str(loop) + '/' + str(len(tweet_list_stream)))
                    write_tweets_list(tweet)
                    if is_write is True:
                        tweet_write_to_file.append(tweet['ID'])
                        yield tweet
                        if tweet['user_id'] not in tracked_user_id_list:
                            tweet_user_write_to_file.append(tweet['user_id'])
                            tracked_user_id_list.append(tweet['user_id'])
                            followers_list = self.tweepy_api_get_followers(tweet['user_id'])
                            for follower in followers_list:
                                followers_list_file.write(str(tweet['user_id']) + '\t' + str(follower) + '\n')
                        else:
                            logging.info("user_id_repeat: " + tweet['user_id'])
                    tweet_list.append(tweet)

                    # yield user
                    # if self.crawl_user:
                except Exception as e:
                    logging.info(str(e))
                    pass
            followers_list_file.write(str(len(tweet_list_stream)))
            followers_list_file.close()
            process_time = (time.time() - start_time)
            logging.info('executation time : ' + str(process_time))

            # 'domain', 'event_name', 'total_participant', 'total_tweets', 'total_enlisted_tweets', 'total_tweets_user', 'time'
            # write system performance information to a csv file
            system_performance.append('sched')
            system_performance.append(sched_item['name'])
            system_performance.append(len(attendees_list))
            system_performance.append(len(tweet_list_stream))
            system_performance.append(len(tweet_write_to_file))
            system_performance.append(len(tweet_user_write_to_file))
            system_performance.append(process_time)
            write_system_performance_info()
            # raise

    def twitter_api_request(self, search):
        api = twitter.Api(consumer_key=CONSUMER_KEY,
                          consumer_secret=CONSUMER_SECRET,
                          access_token_key=ACCESS_TOKEN,
                          access_token_secret=ACCESS_TOKEN_SECRET)
        status = api.GetSearch(search, count=200)
        print(status)

    def tweepy_api_request(self, search_param):
        searched_tweets = [status for status in tweepy.Cursor(api.search, q=search_param, lang='en').items(100)]
        return searched_tweets

    def tweepy_api_get_friends(self, user_id):
        user_friends = [friends for friends in tweepy.Cursor(api.friends_ids, id=user_id).items()]
        return user_friends

    def tweepy_api_get_followers(self, user_id):
        user_followers = [followers for followers in tweepy.Cursor(api.followers_ids, id=user_id).items()]
        return user_followers

    def tweet_ploarity_scoring(self, tweet_text):
        if tweet_text:
            analyzer = SentimentIntensityAnalyzer()
            score = analyzer.polarity_scores(tweet_text)
            print(str(id) + " \ntweet : " + tweet_text + "\nscore{:-<65} {}".format(tweet_text, str(score)))
            return score

    def name_matching(self, matching_list, name, limit):
        match_name = process.extractBests(name, matching_list, limit=limit, score_cutoff=NAME_MATCHING_CUTOFF_VALUE)
        print("Name : " + name + "\nmatch_score : " + str(match_name))
        return match_name

    def save_tweet_list_to_file(self, list, filename='tweet_list'):
        with open(filename, 'wb') as myfile:
            wr = csv.writer(myfile, delimiter=',', quoting=csv.QUOTE_ALL)
            for row in list:
                wr.writerow(row)

    def save_friend_followers_to_file(self, user_id, friend_list, followers_list, event_name):
        with open(event_name + '.csv', 'wb') as myfile:
            wr = csv.writer(myfile, delimiter=',', quoting=csv.QUOTE_ALL)
            for row in friend_list:
                wr.writerow([user_id, row])

    #
    # 1) if (name_match_score or screen_match_score) > 75 and polarity value is positive -----> label=1
    #
    # 2) if (name_match_score or screen_match_score ) < 75 and polarity value is negative -----> label=0
    # ///////condition////////////////////
    #     if (post_polarity is not neutral) then
    #       if (name_matching_score > 75) then
    #           tweet_enlised_with_polarity = [0, 1]
    #
    #       else then
    #       if tweet_is_neg_polarity then
    #           tweet_enlised_with_polarity = [0]
    def is_write_to_file_checking_neutral_first(self, name_score, scrn_name_score, polarity):
        is_write = False
        name_score = name_score[0][1]
        scrn_name_score = scrn_name_score[0][1]
        neu = float(polarity.get('neu'))
        comp = float(polarity.get('compound'))
        if neu != 1 or comp != 0:
            is_full_name_max = True if name_score >= scrn_name_score else False
            max_name_score = name_score if is_full_name_max is True else scrn_name_score
            is_positive, polarity_value = self.check_and_get_polarity_theshold(polarity)
            if max_name_score > NAME_MATCHING_CHECKED_VALUE:
                is_write = True
            else:
                is_write = True if is_positive is False else False
        return is_write, is_positive, polarity_value

    #
    # post_polarity = get_polarity_of_tweets
    # if (name_matching_score > 75) then
    # if (polarity_positive or neutral)
    #     tweet_label = 1
    # else if ( polarity_negative)
    # tweet_label = 0
    #
    # else then
    # if (polarity_negative or neutral)
    #     // As name not matched and neutral or negative comments
    #     tweet_label = 0
    #
    #
    def is_write_to_file_checking_name_first(self, name_score, scrn_name_score, polarity):
        is_write = False
        name_score = name_score[0][1]
        scrn_name_score = scrn_name_score[0][1]
        is_full_name_max = True if name_score >= scrn_name_score else False
        max_name_score = name_score if is_full_name_max is True else scrn_name_score

        if max_name_score > NAME_MATCHING_CHECKED_VALUE:
            is_write = True
            polarity_pole, polarity_value = self.check_and_get_polarity_theshold(polarity)
        else:
            polarity_pole, polarity_value = self.get_polarity_theshold(polarity)
            if not polarity_pole:
                is_write = True

        return is_write, polarity_pole, polarity_value

    # check for bi-polarity either positive or negative
    def check_and_get_polarity_theshold(self, polarity_score):
        pos = float(polarity_score.get('pos'))
        neg = float(polarity_score.get('neg'))
        neu = float(polarity_score.get('neu'))
        comp = float(polarity_score.get('compound'))
        is_positive = True if (pos >= neg) else False
        value = 'pos: ' + str(pos) if is_positive else 'neg: ' + str(neg)
        value = value + ',comp: ' + str(comp)
        return is_positive, value

    # check either neutral or negative
    def get_polarity_theshold(self, polarity_score):
        pos = float(polarity_score.get('pos'))
        neg = float(polarity_score.get('neg'))
        neu = float(polarity_score.get('neu'))
        comp = float(polarity_score.get('compound'))
        is_nn = False if ((neg >= pos) or (neu == 1)) else True
        value = 'neg: ' + str(neg) + ',neu: ' + str(neu) if is_nn else 'pos: ' + str(neu)
        value = value + ',comp: ' + str(comp)
        return is_nn, value


# A new event list crawler for meetup dot com
class TwitterSpider(scrapy.Spider):
    name = 'twitter_meetup'
    start_urls = ["https://twitter.com/search?l=&q=meetup.com%20since:2018-01-01%20until:2018-11-02&src=typd", ]
    attendee_list = []

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url=url, callback=self.parse_twitter, dont_filter=False)

    def parse_twitter(self, response):
        stream = response.xpath(
            "//div[@class='stream']/ol/li/div/div[@class='content']/div[contains(@class,'js-tweet-text-container')]/p/a/@href").extract()
        # stream = response.xpath("//span[contains(@class,'u-block TwitterCardsGrid-col--spacerTop SummaryCard-destination')]").extract()
        print(len(stream))

        urls = re.findall('https?://\S+', ' , '.join(stream))
        for url in urls:
            try:
                r = requests.head(url, allow_redirects=True)
                url = urlparser.urlparse(r.url)
                subdomain_name = url.path.split('/')
                result = re.search(r'(?:[A-Za-z]{2}\-[A-Za-z]{2})', subdomain_name[1])
                event_id = ''
                event_info = ''
                if len(subdomain_name) > 4 and result:
                    event_info = '/' + subdomain_name[2] + '/' + subdomain_name[3] + '/' + subdomain_name[4]
                    event_id = '/' + subdomain_name[2] + '/' + subdomain_name[4]
                elif len(subdomain_name) > 3 and not result:
                    event_info = '/' + subdomain_name[1] + '/' + subdomain_name[2] + '/' + subdomain_name[3]
                    event_id = '/' + subdomain_name[1] + '/' + subdomain_name[3]
                item = ScrapMeetupItem()
                item['name'] = event_info
                if event_id not in url_list:
                    item['repetition'] = 0
                    url_list.append(event_id)
                    home_url = 'https://www.meetup.com' + event_info
                    attendees_url = home_url + '/attendees/'
                    print(attendees_url)
                    yield Request(url=attendees_url, callback=self.parse_meetup_attendees, dont_filter=False,
                                  meta={'item': item})

                    print(self.attendee_list)
            except UnicodeDecodeError:
                print("unicodedecode error:  " + str(url))
                pass

    #
    def parse_meetup_event(self, response):
        item = response.meta['item']
        start_date = response.xpath("//span[@class='dtstart']/span/@title").extract()
        if not start_date:
            start_date = response.xpath("//a[@class='sched-container-anchor']/@id").extract()
        item['date'] = start_date

    def parse_meetup_attendees(self, response):
        if response.status != 404:
            item = response.meta['item']
            subdomain_name = self.get_url_to_subdomain(response.url)
            item['href'] = response.url

            # extract first attendees list
            participents = self.parse_meetup_attendance_list(response)
            item['participant_list'] = len(participents)
            self.attendee_list.append(len(participents))
            print('participant: ' + str(len(participents)))

            # extract pagination information
            no_pages = response.xpath(
                "//div[@class='sched-paging']/a")
            page = 1
            item['current_page'] = page
            last_page_number = 0
            if len(no_pages) > 1:
                last_page_hyperlink = response.xpath("//div[@class='sched-paging']/a/text()").extract()[-1]
                if last_page_hyperlink:
                    last_page_number = int(last_page_hyperlink)
                    print(last_page_number)

            item['total_page'] = last_page_number
            is_last_page = False;

            while page < last_page_number:
                page = page + 1

                req_url = "https://" + subdomain_name + ".sched.com/directory/attendees/" + str(page)
                if page is last_page_number:
                    is_last_page = True
                yield Request(url=req_url, callback=self.parse_sched_pagination, dont_filter=False,
                              meta={'item': item, 'is_last': is_last_page, 'total_page': last_page_number,
                                    'current_page': (page)})
            print("total of " + item['name'] + " count:" + str(item['participant_list']))

    def parse_sched_pagination(self, response):
        if response.status != 404:
            item = response.meta['item']
            participant = self.parse_sched_attendance_list(response)
            item['participant_list'] = item['participant_list'] + len(participant)
            item['current_page'] = item['current_page'] + 1
            total_page = response.meta['total_page']
            if item['current_page'] == total_page:
                print(item)
                yield item

    def parse_meetup_attendance_list(self, response):
        if response.status != 404:
            attendees = response.xpath(
                "//li[@class='attendee-item list-item']/div/div[@class='flex-item']/div/div/a/h4/text()").extract()
            return attendees

    def get_url_to_subdomain(self, url):
        url_new = urlparser.urlparse(url)
        subdomain = url_new.hostname.split('.')[0]
        return subdomain



class TwitterSpider(scrapy.Spider):
    name = 'twitter_follower'
    start_urls = ["https://twitter.com/Expressen/followers", ]
    attendee_list = []

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url=url, callback=self.parse_twitter, dont_filter=False)

    def parse_twitter(self, response):
        stream = response.xpath(
            "//div[@class='stream']/ol/li/div/div[@class='content']/div[contains(@class,'js-tweet-text-container')]/p/a/@href").extract()
        # stream = response.xpath("//span[contains(@class,'u-block TwitterCardsGrid-col--spacerTop SummaryCard-destination')]").extract()
        print(len(stream))

