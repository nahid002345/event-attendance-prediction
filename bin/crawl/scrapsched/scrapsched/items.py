# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy import Item, Field

class ScrapschedItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    href = scrapy.Field()
    name = scrapy.Field()
    date = scrapy.Field()
    twitter_link = scrapy.Field()
    participant_list = scrapy.Field()
    total_page = scrapy.Field()
    current_page = scrapy.Field()
    total_tweets = scrapy.Field()

class ScrapTwitterItem(scrapy.Item):
    href = scrapy.Field()
    name = scrapy.Field()
    date = scrapy.Field()
    twitter_link = scrapy.Field()


class SchedTwitterItem(scrapy.Item):
    name = scrapy.Field()
    date = scrapy.Field()
    twitter_hashtag = scrapy.Field()
    participant_list = scrapy.Field()
    total_page = scrapy.Field()
    current_page = scrapy.Field()
    total_tweets = scrapy.Field()

class Tweet(Item):
    ID = Field()       # tweet id
    event_name = Field()      # tweet url
    # datetime = Field() # post time
    text = Field()     # text content
    user_id = Field()  # user id
    name = Field()          # user name
    screen_name = Field()   # user screen name
    twitter_polarity = Field()
    name_match_score=Field()
    scrn_name_match_score = Field()
    post_label = Field()
    polarity_score = Field()


class User(Item):
    ID = Field()            # user id
    avatar = Field()        # avator url


class ScrapMeetupItem(scrapy.Item):
    href = scrapy.Field()
    name = scrapy.Field()
    date = scrapy.Field()
    twitter_link = scrapy.Field()
    participant_list = scrapy.Field()
    total_page = scrapy.Field()
    current_page = scrapy.Field()
    repetition = scrapy.Field()


class SchedEventTweeterSEItem(scrapy.Item):
    name = scrapy.Field()
    date = scrapy.Field()
    twitter_hashtag = scrapy.Field()
    participant_list = scrapy.Field()
    total_page = scrapy.Field()
    search_engine =scrapy.Field()
    current_page = scrapy.Field()
    total_tweets = scrapy.Field()