# from event_prediction_full import get_event_tweets_matrix,text_pipeline
# # from scrapy import cmdline
# # from util.text_processing import text_token_processed
# # from collections import Counter
from data_validation import data_validation_check, data_twitter_user_followers_list, \
    data_twitter_user_followers_list_twint, get_unextracted_data_id_double_check, \
    create_new_data_edgeList, merge_not_common_and_common_followers_list, merged_edgelist, tweets_from_tweet_id, \
    get_unextracted_tweet_data_id_double_check, score_readibility
# #bb
# from embedd_network import get_graph_from_edge_list
#
# import numpy as np
# from pathlib import Path
# from classifiers import run_classifiers
from EventAttendanceClassifierPipeline import EventAttendanceClassifierPipeline
import argparse
#
# import node2vec
# import twint

# run event prediction with data collection and run evaluation

# event_prediction_main()

# cmdline.execute("scrapy crawl sched".split())
#
# data = get_sentnce_from_file().split('\n')
# token = text_token_processed(data)
# counts = Counter(token)
# print(counts)

# merge_not_common_and_common_followers_list()
# get_unextracted_data_id_double_check()
# data_twitter_user_followers_list_twint()

import os

cur_path = os.path.dirname(__file__)
new_dataset_path = os.path.relpath(os.sep + 'dataset', cur_path)

#
# edge_list_file_name = Path('../dataset/anthrocon.edgelist')
# print(edge_list_file_name)
# text_pipeline()


# embedd_mat = get_graph_from_edge_list(edge_list_file_name)
# embd_mat_shape = embedd_mat.shape
#
# text_mat, text_mat_df = get_event_tweets_matrix()
# text_mat_df['network_embed'] = list(embedd_mat)
# # text_mat = np.squeeze(np.asarray(text_mat))
#
# # text_mat = text_mat_df['text_sentiment'].values.flatten()
# txt_shpe = text_mat.shape
# # net_mat = text_mat_df['network_embed'].values
# # net_shape= net_mat.shape
# combine_mat = np.concatenate((text_mat, embedd_mat), axis=1)
# combine_mat_shape = combine_mat.shape
# feature_label = np.asarray(list(text_mat_df['label']))
# f_label_shape = feature_label.shape
# # np.save(new_dataset_path+ '\\anthrocon.npy',combine_mat)
# print(combine_mat_shape,' ',embd_mat_shape,' ',txt_shpe, 'label_shape ', f_label_shape)
# run_classifiers(combine_mat,feature_label)


# data_twitter_user_followers_list_twint()
# get_unextracted_tweet_data_id_double_check()
# create_new_data_edgeList()
# merged_edgelist()

# tweets_from_tweet_id()

# score_readibility()

parser = argparse.ArgumentParser()

parser.add_argument('-n', '--name', default='anthrocon',
                    help="event name")
parser.add_argument('-i', '--indicator', default='',
                    help="Output file name indicator")
parser.add_argument('-ne', '--net-embd-list', nargs='+', default=['hope','prune','node2vec','walklet'])
parser.add_argument('-s','--start',type=int,default=0,help='net embd starting point')
parser.add_argument('-e','--end',type=int,default=100,help='net embd ending point')
args = parser.parse_args()

event_name = args.name # icann2016 # comiccon2017 #anthrocon
indicator_name=args.indicator
word_embed_list = ['bow', 'tfidf']
net_embed_list = args.net_embd_list
net_embed_list_start = args.start
net_embed_list_end = args.end
classifier_pipeline = EventAttendanceClassifierPipeline(event_name, word_embed_list, net_embed_list,net_embed_list_start=net_embed_list_start,net_embed_list_end=net_embed_list_end,indicator_name=indicator_name)
