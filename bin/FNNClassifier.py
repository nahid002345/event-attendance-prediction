from util.FNN import FNN
from sklearn.base import BaseEstimator, ClassifierMixin
import torch
from torch.autograd import Variable
import torch.nn as nn

class FNNClassifier():
    def __init__(self, min_epoch=5, max_epoch=20, min_batch=50, max_batch=150, train_data_mat=None, learning_rate=0.01,
                 num_class=2):
        self.min_epoch = min_epoch
        self.max_epoch = max_epoch
        self.min_batch = min_batch
        self.max_batch = max_batch
        self.train_data_mat = train_data_mat
        self.learning_rate = learning_rate
        self.num_class = num_class
        self.fnn_net = None
        self.criterion = nn.CrossEntropyLoss()


    def fit(self, X, y):
        display_step = 1
        num_epochs = random.randint(self.min_epoch, self.max_epoch)
        batch_size = random.randint(self.min_batch, self.max_batch)

        feature_size = len(X)
        # Feed-forward Network Parameters
        num_classes = 2  # binary classification of text
        hidden_size = int((feature_size + self.num_class) / 2)  # mean of input and output layer . 1st layer and 2nd layer number of features
        input_size = feature_size
        self.fnn_net = FNN(input_size, hidden_size, num_classes)

        # Loss and Optimizer
        criterion = nn.CrossEntropyLoss()
        optimizer = torch.optim.Adam(self.fnn_net.parameters(), lr=self.learning_rate)

        # Train the Model
        for epoch in range(num_epochs):
            total_batch = int(len(X) / batch_size)
            # Loop over all batches
            for i in range(total_batch):
                training_data = Variable(torch.from_numpy(X.todense().astype("float32")))
                labels = Variable(torch.from_numpy(y.astype("float32")).type(torch.LongTensor))

                # Forward + Backward + Optimize
                optimizer.zero_grad()  # zero the gradient buffer
                outputs = self.fnn_net(training_data)
                loss = criterion(outputs, labels)
                loss.backward()
                optimizer.step()

                if (i + 1) % 4 == 0:
                    print('Epoch [%d/%d], Step [%d/%d], Loss: %.4f'
                          % (epoch + 1, num_epochs, i + 1, len(train_x.data) // batch_size, loss.item()))

        # Test the Model & Evaluation
        training_data = Variable(torch.from_numpy(X.astype("float32")))
        labels = torch.from_numpy(y.astype("float32")).type(torch.LongTensor)
        outputs = net(training_data)
        _, predicted = torch.max(outputs.data, 1)
#     xtrain_tfidf_ngram = tfidf_vect_ngram.transform(train_x)
#     xvalid_tfidf_ngram = tfidf_vect_ngram.transform(valid_x)
#
#     # Parameters
#     words_vocab_num = len(tfidf_vect_ngram.vocabulary_)
#     print_result.append(words_vocab_num)
#
#     learning_rate = 0.01
#     num_epochs = random.randint(MIN_NUM_EPOCH, MAX_NUM_EPOCH)
#     batch_size = random.randint(MIN_BATCH_SIZE, MAX_BATCH_SIZE)
#     display_step = 1
#
#     print_result.append(learning_rate)
#     print_result.append(num_epochs)
#     print_result.append(batch_size)
#     # Feed-forward Network Parameters
#     num_classes = 2  # binary classification of text
#     hidden_size = int(
#         (
#                 words_vocab_num + num_classes) / 2)  # mean of input and output layer . 1st layer and 2nd layer number of features
#     input_size = words_vocab_num  # Words in vocab
#     print_result.append(hidden_size)
#     net = FNN(input_size, hidden_size, num_classes)
#
#     # Loss and Optimizer
#
#     criterion = nn.CrossEntropyLoss()
#     optimizer = torch.optim.Adam(net.parameters(), lr=learning_rate)
#
#     # Train the Model
#     for epoch in range(num_epochs):
#         total_batch = int(len(train_x.data) / batch_size)
#         # Loop over all batches
#         for i in range(total_batch):
#             articles = Variable(torch.from_numpy(xtrain_tfidf_ngram.todense().astype("float32")))
#             labels = Variable(torch.from_numpy(train_y.astype("float32")).type(torch.LongTensor))
#
#             # Forward + Backward + Optimize
#             optimizer.zero_grad()  # zero the gradient buffer
#             outputs = net(articles)
#             loss = criterion(outputs, labels)
#             loss.backward()
#             optimizer.step()
#
#             if (i + 1) % 4 == 0:
#                 print('Epoch [%d/%d], Step [%d/%d], Loss: %.4f'
#                       % (epoch + 1, num_epochs, i + 1, len(train_x.data) // batch_size, loss.item()))
#
#     # Test the Model & Evaluation
#     total_test_data = len(valid_x.data)
#     articles = Variable(torch.from_numpy(xvalid_tfidf_ngram.todense().astype("float32")))
#     labels = torch.from_numpy(valid_y.astype("float32")).type(torch.LongTensor)
#     outputs = net(articles)
#     _, predicted = torch.max(outputs.data, 1)
#     accuracy, precision, recall, f1_measure = f1_score(labels, predicted)
#
#     process_time = (time.time() - start_time)
#     # print result in csv file
#     print_result.append(float(accuracy.float()))
#     print_result.append(float(precision.float()))
#     print_result.append(float(recall.float()))
#     print_result.append(float(f1_measure.float()))
#     print_result.append(process_time)
#
#     df = pd.DataFrame(
#         columns=['dataset', 'vocab', 'learning_rate', 'epoch', 'batch', 'hidden', 'accuracy', 'precision', 'recall',
#                  'f1',
#                  'time'])
#     df.loc[0] = print_result
#
#     df.to_csv('results.csv', sep=',', header=False, index=True, mode='a')
