from data_import import data_csv_import_array, data_csv_import_array_by_col_index, data_txt_import_list, \
    convert_list_to_dict, write_line_in_file, remove_duplicates, convert_dict_to_list, write_json_in_file, \
    write_csv_in_file
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
from util.text_processing import string_token_process, common_item_in_two_list, remove_duplication_from_arry,remove_hyperlink_text,strip_links,remove_urls,strip_all_entities
import logging
import csv
import tweepy
import twint
from datetime import datetime
import time
import json
import unicodecsv
import pandas as pd
import textstat
from langdetect import detect, detect_langs
import re

logging.getLogger().setLevel(logging.INFO)

CONSUMER_KEY = 'oMOgH9nBmJuvhowTzPij2pYIZ'
CONSUMER_SECRET = 'VFkZrUL5UpMLJ9hhImpYxHmuKKznCU9ngMZgdZBZnMvolZyPEd'
ACCESS_TOKEN = '38574750-2nF0nmmwZAI1BJuGvJOgacWYp7oMnzsJ7zhxQWh1F'
ACCESS_TOKEN_SECRET = 'LKnOh7v74LIdXkklF7KIPOThxBcLymP7bWzRm5teEMYJS'

LIST_ITEM = 0
CONSUMER_KEY_LIST = ['s111GvSm0xIuq9QmQTd3mpDwG',
                     'mmtZjoNlXtsHc0VWXeTVZ3BDy',
                     'qFctHXUMsnAuZiFBFwpZ3ouSs',
                     'JyvBMPTm899yikT3ZledzhGWU',
                     'p5liVMN4mcNpMguOCdYQGvQXj',
                     'TiRj6Wzyq3MxU0qOzI4VtYooF',
                     '8NYK7JttzzJDoNP0GAXsDFyOl',
                     '1NZtwAiwXPHbc1iZPay8ObF7n',
                     'JURfn9qYe6YD7Qil4hD76qGRC',
                     '5EwRHUxlG9USJ7SIBAUeovJ2z',
                     'RSeKHwX7S25Tjd2q4YQXDtVdT', ]

CONSUMER_SECRET_LIST = ['aosjQyRuR7np28SLpX6MyAVCeap3cfJ36stMrsblRQSRc5L7G0',
                        'FXwsLE4Gy3sedoLEA5Fa5p0y48qLbhpZrPy1kP9zMZaccVwCJV',
                        'pGCIywE61YjoYgrFwHlKOFvBABIb1f9IZxI9mqhtx9fO0q8vvS',
                        's6ukLSc1zdsnGYM8y9bnYz9eLhZBogDs50kbYuTC7TGd8noeW9',
                        'dFXBv4AoZMznNSb8qkpBVv6Th1CBu1DFBzCI1QAdqQJ3xLqb52',
                        'LHEd6XgIHMMrhH6niqlvFFtuzWAlS5jArX4drM8whfX4KPuqbi',
                        'Q29BWAORyLDernTWde1EerYnDapsxLD28k6hCr9tzCY5mMMy13',
                        '2fGOAfjVnNLN1Cgsql7bv6w0RKFVSbtNa0J6JNjt7GuurCb2B4',
                        'r5qRBTKUQKAG73GM2D9yQwWTlvumgxR5FYC31GeFDhXA3zqXF9',
                        'pcnJoo33S0eHWrmZDUG4qRvkJy0eOxbVDlie4FFA8X61y20Sza',
                        'WFOdBMxxanYtMsD38bdD7osbpr6IvcKeGpCK5pBmke3FXYeONd', ]

ACCESS_TOKEN_LIST = ['38574750-0A7vRI5YrUEjiKP0NPvapkughooUQ5PWAY3xLqb7E',
                     '38574750-vJCJNj1BCTXW1u96AbSjsw9J8HjpUwNPBLqRNqbJw',
                     '38574750-BOuHGDhNy1UT7DdFAKYRg9bm8FglLq5oLgdaVN9GK',
                     '38574750-x4fjwQe2W7ImvQSP1XTzbTwYFuc1LcZ0dvyE0xPyi',
                     '38574750-oT6DsQHmMyjdSckpNKJMn5i3AxNUljx7W3LRGkwnr',
                     '38574750-6ieXNWXAO4QWdbwQcU8J2chr17GPRly77ddzxdBJW',
                     '38574750-WKwDaNrY47VqbFQiRQ2sMgnhVUuwBMsioCdyQz5W0',
                     '38574750-v7vG1s51HcJvA9vFHkgem0caVQ0lXNKchjIvGjpFI',
                     '38574750-IW0nFLGKf3sg1tiNHsUH1IPuqYH9xr1m22p8oXxP5',
                     '38574750-VHsKuIRaTPstVl47nNXiuOkYqLOiDaEs5GS1yDV4Y',
                     '724006529238732800-kBYH6605ms7Qq532VIBWRMKX6z5k7kZ', ]

ACCESS_TOKEN_SECRET_LIST = ['OXq0cxAJUSA4y7VD8hAJ9lIpiV2A3kMaDfdMhYLfKmk1w',
                            '3zNHrXyEN8qs9X875Ix4J0moK0WZ1t7lilBSIYaer9yVv',
                            'YJEoZyArIpUuQSQbR5iXdUOYUeNNEFoMG91w5FwChGeHs',
                            'HlriGVHU5okhHoP8hDh0XVoVAKa1TS71zXaGvimI4Ui0M',
                            'jN5ThCbWpgcv1rQXCB0Qxqx861DNZTqLFjJqlDFUtHdBR',
                            'jCRXGK0StVdt085CVOtRa6lGSFLhHOLCxd7lt7zmUVwjv',
                            'uD3mt8TLjutl9iH97zfuuwrN5dbpGfvX2hLl4QB1mfLyb',
                            'zWhB5QcU6tbA9aIgaYPvSCBlHU7VCpEX63Gt30HxSlmqR',
                            'tvXNIuyjEtz4odCFT4TpFRXY1SqB6TK4MLv3SaNCbOIX7',
                            'Cfrd9eG3ajVDHsku97iGhEuzdwqyLSGISDjGfBVWxUsjb',
                            '6inQyTojdhd7Uk8Wbiwev2XVbomaNW6p2wbqdkzmcYzGk', ]

# auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
# auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
# # api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)
# api = tweepy.API(auth)
EXTRACTED_FILE_NAME = 'filtered_dataset/icann2016_tweets'
EXISTING_FOLLOWERS_FILE = "filtered_dataset/bitconference_followers_edge_list.txt"

NON_ATTENDED_WORDS = ["miss",
                      "wish",
                      "can't",
                      "hate",
                      "don't",
                      "regret",
                      "there",
                      "not",
                      "sell"]

ATTENDED_WORDS = ["come",
                  "go",
                  "participate",
                  "register",
                  "login",
                  "join",
                  "check",
                  "check in",
                  "attend",
                  "enjoy",
                  "excite",
                  "excited",
                  "amazing",
                  "day",
                  "days",
                  "good",
                  "thank",
                  "thanks",
                  "thankful",
                  "like",
                  "see",
                  "fun",
                  "booked",
                  "buy",
                  "hang out",
                  "will"
                  "will be",
                  "count",
                  "visit",
                  "present",
                  "lucky", ]

NAME_MATCHING_SCORE_HIGH_THRESHOLD = 90
NAME_MATCHING_SCORE_MEDIUM_THRESHOLD = 65
NAME_MATCHING_SCORE_HIGH = 2
NAME_MATCHING_SCORE_MEDIUM = 1
NAME_MATCHING_SCORE_LOW = 0

SENTIMENT_HIGH_THRESHOLD = 0.75

CSV_COLUMN_LABEL = 15
CSV_COLUMN_NAME_MATCH_SCORE = 9
CSV_COLUMN_TWEET_TEXT = 14
CSV_COLUMN_TWEET_SENT_NEG = 10
CSV_COLUMN_TWEET_SENT_NEU = 11
CSV_COLUMN_TWEET_SENT_POS = 12
CSV_COLUMN_TWEET_SENT_COMP = 13

CSV_COLUMN_TWEET_USER_ID = 1
CSV_COLUMN_TWEET_SENTENCE = 14


def tweet_ploarity_scoring(tweet_text):
    analyzer = SentimentIntensityAnalyzer()
    score = analyzer.polarity_scores(tweet_text)
    print(str(id) + " \ntweet : " + tweet_text + "\nscore{:-<65} {}".format(tweet_text, str(score)))
    return score


def write_data_csv(data_list):
    writer = csv.writer(open(EXTRACTED_FILE_NAME + '_filtered.csv', 'w', encoding='utf-8'))
    for data in data_list:
        writer.writerow(data)


# status against a score considering by high and medium threshold
# HIGH=1, MEDIUM
def get_name_matching_status(score):
    status = 0
    if score > NAME_MATCHING_SCORE_HIGH_THRESHOLD:
        status = NAME_MATCHING_SCORE_HIGH
    elif NAME_MATCHING_SCORE_MEDIUM_THRESHOLD < score <= NAME_MATCHING_SCORE_HIGH_THRESHOLD:
        status = NAME_MATCHING_SCORE_MEDIUM
    else:
        status = NAME_MATCHING_SCORE_LOW

    return status


def data_validation_check():
    csv_data = data_csv_import_array(EXTRACTED_FILE_NAME + ".csv", ';')
    loop = 0
    filter_data = []
    for data in csv_data:
        try:
            loop += 1
            tweet_label = data[CSV_COLUMN_LABEL]
            name_matching_score = int(data[CSV_COLUMN_NAME_MATCH_SCORE])
            tweet_text = data[CSV_COLUMN_TWEET_TEXT]
            # if no sentiment found, then insert it in existing data
            if not data[CSV_COLUMN_TWEET_SENT_NEG]:
                sentiment_score = tweet_ploarity_scoring(tweet_text)
                tweet_pos_sent = float(sentiment_score.get('pos'))
                tweet_neg_sent = float(sentiment_score.get('neg'))

                data[CSV_COLUMN_TWEET_SENT_NEG] = sentiment_score.get('neg')
                data[CSV_COLUMN_TWEET_SENT_NEU] = sentiment_score.get('neu')
                data[CSV_COLUMN_TWEET_SENT_POS] = sentiment_score.get('pos')
                data[CSV_COLUMN_TWEET_SENT_COMP] = sentiment_score.get('compound')
            else:
                tweet_neg_sent = float(data[CSV_COLUMN_TWEET_SENT_NEG])
                tweet_pos_sent = float(data[CSV_COLUMN_TWEET_SENT_POS])
            if name_matching_score:
                status = get_name_matching_status(name_matching_score)
                # HIGH > HIGH THRESHOLD VALUE, NAME_MATCHING_SCORE_MEDIUM_THRESHOLD < MEDIUM <= HIGH THRESHOLD VALUE , LOW < NAME_MATCHING_SCORE_MEDIUM_THRESHOLD
                if status == NAME_MATCHING_SCORE_HIGH:
                    if tweet_label == '0':
                        if tweet_neg_sent > SENTIMENT_HIGH_THRESHOLD:
                            tokens = string_token_process(tweet_text)
                            is_common_negative = common_item_in_two_list(tokens, NON_ATTENDED_WORDS)
                            is_common_positive = common_item_in_two_list(tokens, ATTENDED_WORDS)
                            if is_common_negative and not is_common_positive:
                                data.append('0')
                            else:
                                data.append('1')
                        else:
                            data.append('1')
                    else:
                        data.append('1')
                elif status == NAME_MATCHING_SCORE_MEDIUM:
                    tokens = string_token_process(tweet_text)
                    if tweet_label == '1':
                        is_common_negative = common_item_in_two_list(tokens, NON_ATTENDED_WORDS)
                        is_common_positive = common_item_in_two_list(tokens, ATTENDED_WORDS)
                        if is_common_negative and not is_common_positive:
                            data.append('0')
                        else:
                            data.append('1')
                    else:
                        is_common_negative = common_item_in_two_list(tokens, NON_ATTENDED_WORDS)
                        is_common_positive = common_item_in_two_list(tokens, ATTENDED_WORDS)
                        if is_common_positive and not is_common_negative:
                            data.append('1')
                        else:
                            data.append('0')
                else:
                    if tweet_label == '1':
                        if tweet_pos_sent > SENTIMENT_HIGH_THRESHOLD:
                            tokens = string_token_process(tweet_text)
                            is_common_negative = common_item_in_two_list(tokens, NON_ATTENDED_WORDS)
                            is_common_positive = common_item_in_two_list(tokens, ATTENDED_WORDS)
                            if is_common_positive and not is_common_negative:
                                data.append('1')
                            else:
                                data.append('0')
                        else:
                            data.append('0')
                    else:
                        data.append('0')
                filter_data.append(data)
        except Exception as e:
            logging.error(e)
            logging.info('loop : ' + str(loop))
            pass

    write_data_csv(filter_data)


def data_twitter_user_followers_list():
    csv_data = data_csv_import_array_by_col_index(EXTRACTED_FILE_NAME + ".csv", CSV_COLUMN_TWEET_USER_ID, ';')
    loop = 0
    filter_data = []
    twitter_user_list_str = remove_duplication_from_arry(csv_data)
    exisiting_user_followers_dict = get_data_from_existing_followers_file(
        "filtered_dataset/anthrocon_followers_edge_list.txt")
    twitter_user_list = [int(follower_id) for follower_id in twitter_user_list_str]
    event_name = EXTRACTED_FILE_NAME.split('/')[1]
    followers_list_file = open(event_name + '_followers_edge_list.txt', 'w')
    followers_not_enlisted = open(event_name + '_followers_not_listed.txt', 'w')

    dict_keys_list = sorted(exisiting_user_followers_dict)
    common_users_list = list(set(dict_keys_list) & set(twitter_user_list))

    for data in common_users_list:
        try:
            followers_list = exisiting_user_followers_dict.get(data)
            common_followers = list(set(followers_list) & set(twitter_user_list))
            for follower in common_followers:
                followers_list_file.write(str(data) + ' ' + str(follower) + '\n')
            if len(common_followers) == 0:
                followers_not_enlisted.write(str(data) + '\n')
        except Exception as e:
            logging.error(e)
            logging.info('loop : ' + str(loop))
            pass

    for data in sorted(twitter_user_list):
        try:
            loop += 1
            logging.info('loop : ' + str(loop) + '/' + str(len(twitter_user_list)) + ' data: ' + str(data))

            followers_list = tweepy_api_get_followers(data)
            common_followers = list(set(followers_list) & set(twitter_user_list))

            for follower in common_followers:
                followers_list_file.write(str(data) + ' ' + str(follower) + '\n')
            if len(common_followers) == 0:
                followers_not_enlisted.write(str(data) + '\n')
        except Exception as e:
            logging.error(e)
            logging.info('loop : ' + str(loop))
            pass
    followers_list_file.close()
    followers_not_enlisted.close()


def data_twitter_user_followers_list_twint():
    csv_data = data_csv_import_array_by_col_index(EXTRACTED_FILE_NAME + ".csv", CSV_COLUMN_TWEET_USER_ID, ',')
    loop = 0
    filter_data = []
    twitter_user_list_str = remove_duplication_from_arry(csv_data)
    exisiting_user_followers_dict = get_data_from_existing_followers_file(EXISTING_FOLLOWERS_FILE)
    twitter_user_list = [int(follower_id) for follower_id in twitter_user_list_str]
    event_name = EXTRACTED_FILE_NAME.split('/')[1]
    current_time = str(datetime.now())
    starting_msg = '################### ' + current_time + '#################' + '\n'

    write_line_in_file(event_name + '_followers_edge_list.txt', 'a+', starting_msg)
    write_line_in_file(event_name + '_followers_not_listed.txt', 'a+', starting_msg)
    write_line_in_file(event_name + '_followers_extracted_info.txt', 'a+', starting_msg)
    write_line_in_file(event_name + '_followers_extracted.txt', 'a+', starting_msg)

    dict_keys_list = sorted(exisiting_user_followers_dict)
    common_users_list = list(set(dict_keys_list) & set(twitter_user_list))
    #
    # for data in common_users_list:
    #     try:
    #         followers_list = exisiting_user_followers_dict.get(data)
    #         common_followers = list(set(followers_list) & set(twitter_user_list))
    #         for follower in common_followers:
    #             write_line_in_file(event_name + '_followers_edge_list.txt', 'a+',
    #                                str(data) + ' ' + str(follower) + '\n')
    #         if len(common_followers) == 0:
    #             write_line_in_file(event_name + '_followers_not_listed.txt', 'a+', str(data) + '\n')
    #     except Exception as e:
    #         logging.error(e)
    #         logging.info('loop : ' + str(loop))
    #         pass

    extract_only_tweepy_followers_id(event_name, twitter_user_list, [])


def extract_followers_id(event_name, twitter_user_list, full_user_list):
    loop = 703
    for data in sorted(twitter_user_list)[loop:]:
        try:
            loop += 1
            logging.info('loop : ' + str(loop) + '/' + str(len(twitter_user_list)) + ' data: ' + str(data))
            is_tweepy = 1
            total_attempt = 0
            while (total_attempt < len(CONSUMER_KEY_LIST)):
                request_api = tweepy_api_setup(CONSUMER_KEY_LIST[total_attempt], CONSUMER_SECRET_LIST[total_attempt],
                                               ACCESS_TOKEN_LIST[total_attempt],
                                               ACCESS_TOKEN_SECRET_LIST[total_attempt])
                followers_list = tweepy_api_get_followers(request_api, data)
                logging.info('attempt: ' + str(total_attempt))
                if len(followers_list) != 0:
                    logging.info('success attempt: ' + str(total_attempt))
                    break;
                else:
                    total_attempt += 1

            if len(followers_list) == 0:
                is_tweepy = 0
                followers_list_unfiltered = get_user_follower_list_twint(data)
                followers_list = [item for item in followers_list_unfiltered if item.isdigit() and len(str(item)) > 4]

            common_followers = list(set(followers_list) & set(full_user_list))
            write_line_in_file(event_name + '_followers_extracted_info.txt', 'a+',
                               str(data) + '\t' + str(len(followers_list)) + '\t' + str(is_tweepy) + ' ' + str(
                                   loop) + '\n')
            for follower in followers_list:
                write_line_in_file(event_name + '_followers_extracted.txt', 'a+',
                                   str(data) + ' ' + str(follower) + ' ' + str(is_tweepy) + ' ' + str(loop) + '\n')

            for follower in common_followers:
                write_line_in_file(event_name + '_followers_edge_list.txt', 'a+',
                                   str(data) + ' ' + str(follower) + ' ' + str(is_tweepy) + ' ' + str(loop) + '\n')
            if not common_followers or len(common_followers) == 0:
                write_line_in_file(event_name + '_followers_not_listed.txt', 'a+', str(data) + ' ' + str(loop) + '\n')
        except Exception as e:
            logging.error(e)
            logging.info('loop : ' + str(loop))
            pass


def extract_only_tweepy_followers_id(event_name, twitter_user_list, full_user_list):
    sleeping_time = (5 * 60)
    start_node = 0
    end_node = len(twitter_user_list)

    loop = start_node
    for data in sorted(twitter_user_list)[start_node:end_node]:
        total_try = 0
        try:
            loop += 1
            logging.info('loop : ' + str(loop) + '/' + str(len(twitter_user_list)) + ' data: ' + str(data))
            is_tweepy = 1
            total_attempt = 0
            start_time = time.time()
            while (total_attempt < len(CONSUMER_KEY_LIST)):
                request_api = tweepy_api_setup(CONSUMER_KEY_LIST[total_attempt], CONSUMER_SECRET_LIST[total_attempt],
                                               ACCESS_TOKEN_LIST[total_attempt],
                                               ACCESS_TOKEN_SECRET_LIST[total_attempt])
                followers_list = tweepy_api_get_followers(request_api, data, True)
                logging.info('attempt: ' + str(total_attempt))
                if len(followers_list) != 0:
                    logging.info('success attempt: ' + str(total_attempt))
                    break;
                else:
                    total_attempt += 1

                if len(followers_list) == 0 and total_attempt == len(CONSUMER_KEY_LIST) and total_try <= 3:
                    total_try += 1
                    total_attempt = 0
                    scrap_time = time.time() - start_time
                    next_api_time = sleeping_time - scrap_time
                    # logging.info('sleeping the extracting (in sec): ' + str(sleeping_time))
                    # time.sleep(sleeping_time)

            # if len(followers_list) == 0 and total_attempt > 0:
            #     is_tweepy = 0
            #     followers_list_unfiltered = get_user_follower_list_twint(data)
            #     followers_list = [item for item in followers_list_unfiltered if item.isdigit() and len(str(item)) > 4]
            #     scrap_time = time.time() - start_time
            #     next_api_time = sleeping_time - scrap_time
            #     # if next_api_time > 0:
            #     #     logging.info('sleeping the extracting (in sec): ' + str(next_api_time))
            #     #     time.sleep(next_api_time)

            common_followers = list(set(followers_list) & set(full_user_list))
            write_line_in_file(event_name + '_followers_extracted_info.txt', 'a+',
                               str(data) + '\t' + str(len(followers_list)) + '\t' + str(is_tweepy) + ' ' + str(
                                   loop) + '\n')
            for follower in followers_list:
                write_line_in_file(event_name + '_followers_extracted.txt', 'a+',
                                   str(data) + ' ' + str(follower) + ' ' + str(is_tweepy) + ' ' + str(loop) + '\n')

            for follower in common_followers:
                write_line_in_file(event_name + '_followers_edge_list.txt', 'a+',
                                   str(data) + ' ' + str(follower) + ' ' + str(is_tweepy) + ' ' + str(loop) + '\n')
            if not common_followers or len(common_followers) == 0:
                write_line_in_file(event_name + '_followers_not_listed.txt', 'a+', str(data) + ' ' + str(loop) + '\n')
        except Exception as e:
            logging.error(e)
            logging.info('loop : ' + str(loop))
            pass


def get_user_follower_list_twint(user_id):
    followers_user_id_list = []
    try:
        c = twint.Config()
        c.User_id = str(user_id)
        c.Store_object = True
        twint.run.Followers(c)
    except Exception as e:
        logging.error(e)
        pass

    tlist = twint.run.output._follow_list
    list = [item for sublist in twint.run.output._follow_list]
    return [item for sublist in twint.run.output._follow_list for item in sublist]


def get_unextracted_data_id_double_check():
    event_name = 'supervised_dataset_vfestival_asonam2017'

    # get full user list
    csv_data = data_csv_import_array_by_col_index(EXTRACTED_FILE_NAME + ".csv", CSV_COLUMN_TWEET_USER_ID, ',')
    twitter_user_list_str = remove_duplication_from_arry(csv_data)
    full_user_list = [int(follower_id) for follower_id in twitter_user_list_str]
    #

    file_followers_not_listed = 'supervised_dataset_vfestival_asonam2017_tweets_list_followers_not_listed.txt'
    file_followers_zero_extracted_info = 'supervised_dataset_vfestival_asonam2017_tweets_list_followers_extracted_info.txt'
    followers_not_listed_list = remove_duplicates(data_txt_import_list(file_followers_not_listed, ' '))
    followers_not_listed_flattened_list = [y for x in followers_not_listed_list for y in x]

    file_followers_zero_extracted_info = data_txt_import_list(file_followers_zero_extracted_info, '\t')
    file_followers_zero_extracted_info_user_label_dict = convert_list_to_dict(file_followers_zero_extracted_info, 2, 0)
    file_followers_zero_extracted_info_user_count_dict = convert_list_to_dict(file_followers_zero_extracted_info, 0, 1)
    file_followers_zero_extracted_info_dict_list = list(file_followers_zero_extracted_info_user_label_dict.get('1'))

    file_already_extracted_data = 'supervised_dataset_vfestival_asonam2017_followers_extracted.txt'
    already_extracted_data = data_txt_import_list(file_already_extracted_data, '\t')
    already_extracted_data_dict = convert_list_to_dict(already_extracted_data, 0, 1)

    already_extracted_data_list = list(map(int, sorted(already_extracted_data_dict.keys())))

    common_id_list = list(set(file_followers_zero_extracted_info_dict_list) & set(followers_not_listed_flattened_list))
    print(common_id_list)
    total_list = remove_duplicates(file_followers_zero_extracted_info_dict_list + common_id_list)
    print(len(total_list))
    total_list = list(set(total_list) ^ set(already_extracted_data_list))
    unextracted_list = []
    for data_id in total_list:
        followers_count = 0
        if data_id in file_followers_zero_extracted_info_user_count_dict.keys():
            followers_count = file_followers_zero_extracted_info_user_count_dict.get(data_id)

        if followers_count < 10000:
            unextracted_list.append(data_id)

    print(len(unextracted_list))
    extract_only_tweepy_followers_id(event_name, unextracted_list, full_user_list)


# get unextracted data from checking csv file and extracted list
def get_unextracted_tweet_data_id_double_check():
    event_name = 'supervised_dataset_vfestival_asonam2017'

    # get full user list
    csv_data = data_csv_import_array_by_col_index(EXTRACTED_FILE_NAME + ".csv", CSV_COLUMN_TWEET_USER_ID, ',')
    twitter_user_list_str = remove_duplication_from_arry(csv_data)
    full_user_list = [int(follower_id) for follower_id in twitter_user_list_str]
    #
    file_followers_not_listed = 'supervised_dataset_vfestival_asonam2017_tweets_list_followers_not_listed.txt'
    file_followers_zero_extracted_info = 'supervised_dataset_vfestival_asonam2017_tweets_list_followers_extracted_info.txt'
    followers_not_listed_list = remove_duplicates(data_txt_import_list(file_followers_not_listed, ' '))
    followers_not_listed_flattened_list = [y for x in followers_not_listed_list for y in x]

    file_followers_zero_extracted_info = data_txt_import_list(file_followers_zero_extracted_info, '\t')
    file_followers_zero_extracted_info_user_label_dict = convert_list_to_dict(file_followers_zero_extracted_info, 2, 0)
    file_followers_zero_extracted_info_user_count_dict = convert_list_to_dict(file_followers_zero_extracted_info, 0, 1)
    file_followers_zero_extracted_info_dict_list = list(file_followers_zero_extracted_info_user_label_dict.get('1'))

    file_already_extracted_data = 'supervised_dataset_vfestival_asonam2017_tweets_list_followers_extracted.txt'
    already_extracted_data = data_txt_import_list(file_already_extracted_data, '\t')
    already_extracted_data_dict = convert_list_to_dict(already_extracted_data, 0, 1)

    already_extracted_data_list = list(map(int, sorted(already_extracted_data_dict.keys())))

    total_list = list(set(full_user_list) ^ set(already_extracted_data_list))
    unextracted_list = []
    for data_id in total_list:
        followers_count = 0
        if data_id in file_followers_zero_extracted_info_user_count_dict.keys():
            followers_count = file_followers_zero_extracted_info_user_count_dict.get(data_id)

        if followers_count < 10000:
            unextracted_list.append(data_id)

    print(len(unextracted_list))
    extract_only_tweepy_followers_id(event_name, unextracted_list, [])


def get_data_from_existing_followers_file(filename):
    exisiting_data_list = data_txt_import_list(filename, ' ')
    exisiting_followers_dict = convert_list_to_dict(exisiting_data_list)
    return exisiting_followers_dict


def tweepy_api_setup(consum_key, consum_secret, access_key, access_secret):
    auth = tweepy.OAuthHandler(consum_key, consum_secret)
    auth.set_access_token(access_key, access_secret)
    # api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)
    api = tweepy.API(auth)
    return api


def tweepy_api_get_followers(current_api, user_id, is_wait=False):
    user_followers = []
    try:
        user_followers = [followers for followers in
                          tweepy.Cursor(current_api.followers_ids, id=user_id, count=5000).items()]
    except tweepy.TweepError as e:
        user_followers = []
        logging.error(e)
        pass
    return user_followers


def tweepy_api_get_tweets_status(current_api, tweet_list):
    tweets_list = []
    try:
        tweets_list = current_api.statuses_lookup(tweet_list, include_entities=True)
    except tweepy.TweepError as e:
        tweets_list = []
        logging.error(e)
        pass
    return tweets_list


def create_new_data_edgeList():
    file_followers_edge = 'anthrocon_tweet_validation_filter_label_list_followers_edge_list.txt'
    file_followers_edge_list = data_txt_import_list(file_followers_edge, '\t')

    file_followers_edge_list_dict = convert_list_to_dict(file_followers_edge_list, 0, 1)
    file_followers_list = list(file_followers_edge_list_dict.keys())
    for user_id in file_followers_list:
        followers_id_list = remove_duplicates(file_followers_edge_list_dict.get(user_id))
        for follower_id in followers_id_list:
            write_line_in_file('anthrocon_edge_list.txt', 'a+', str(user_id) + ' ' + str(follower_id) + '\n')
    print(len(file_followers_edge_list_dict.keys()))


def merge_not_common_and_common_followers_list():
    event_name = 'anthrocon'
    edge_list_file_name = event_name + '_edge_list.txt'
    no_common_file_name = event_name + '_followers_not_listed.txt'

    edeg_list = data_txt_import_list(edge_list_file_name)
    no_common_followers_list = data_txt_import_list(no_common_file_name)

    # get full user list
    csv_data = data_csv_import_array_by_col_index(EXTRACTED_FILE_NAME + ".csv", CSV_COLUMN_TWEET_USER_ID, ';')
    twitter_user_list_str = remove_duplication_from_arry(csv_data)
    full_user_list = [int(follower_id) for follower_id in twitter_user_list_str]

    edge_list_dict = convert_list_to_dict(edeg_list, 0, 1)
    user_followers_list = list(edge_list_dict.keys())
    for item in full_user_list:
        if str(item) not in user_followers_list:
            edge_list_dict[str(item)] = int(item)

    new_edge_list = convert_dict_to_list(edge_list_dict)
    print(len(new_edge_list))
    for user_id in new_edge_list:
        is_list = isinstance(user_id[1], list)
        if is_list:
            for followers_id in list(user_id[1]):
                # print('list: '+str(user_id[0]) + ' ' + str(followers_id) + '\n')
                write_line_in_file(event_name + '_full_edge_list.txt', 'a+',
                                   str(user_id[0]) + ' ' + str(followers_id) + '\n')
        else:
            # print(str(user_id[0]) + ' ' + str(user_id[1]) + '\n')
            write_line_in_file(event_name + '_full_edge_list.txt', 'a+', str(user_id[0]) + ' ' + str(user_id[1]) + '\n')


def merged_edgelist():
    file_already_extracted_data = 'supervised_dataset_vfestival_asonam2017_tweets_list_followers_extracted.txt'
    already_extracted_data = data_txt_import_list(file_already_extracted_data, '\t')
    already_extracted_data_dict = convert_list_to_dict(already_extracted_data, 0, 1)
    already_extracted_user_list = list(map(int, sorted(already_extracted_data_dict.keys())))

    csv_data = data_csv_import_array_by_col_index(EXTRACTED_FILE_NAME + ".csv", CSV_COLUMN_TWEET_USER_ID, ',')
    twitter_user_list_str = remove_duplication_from_arry(csv_data)
    twitter_user_list = [int(follower_id) for follower_id in twitter_user_list_str]
    common_id_list = []
    for user_id in twitter_user_list:
        f_list = already_extracted_data_dict.get(str(user_id))
        if f_list is not None:
            followers_id_list = remove_duplicates(f_list)
            common_id_list = list(
                set(already_extracted_user_list) & set(followers_id_list))

        for follower_id in common_id_list:
            write_line_in_file('supervised_dataset_vfestival_asonam2017_edge_list.txt', 'a+',
                               str(user_id) + ' ' + str(follower_id) + '\n')
        if (len(common_id_list) == 0):
            write_line_in_file('supervised_dataset_vfestival_asonam2017_edge_list.txt', 'a+',
                               '#' + str(user_id) + ' ' + str(user_id) + '\n')


##get tweets from tweet_id and create new edgelist
def tweets_from_tweet_id():
    file_name = 'supervised_dataset_vfestival_asonam2017'
    csv_data = data_csv_import_array_by_col_index(file_name + ".csv", 0, ';')
    twitter_id_list = remove_duplication_from_arry(csv_data)
    full_tweet_id_list = [int(tweet_d) for tweet_d in twitter_id_list]
    print(len(full_tweet_id_list))

    sleeping_time = (15 * 60)
    start_node = 0
    sliced_step = 90
    end_node = sliced_step

    loop = start_node
    csvfile = open(file_name + '_tweets_list.csv', 'a+', encoding='utf-8')
    csvwriter = csv.writer(csvfile)
    while (start_node < len(full_tweet_id_list)):
        print('start:' + str(start_node) + ' end:' + str(end_node))
        sliced_tweets_id_list = full_tweet_id_list[start_node:end_node]
        start_node += (sliced_step + 1)
        if (len(full_tweet_id_list) - end_node) > sliced_step:
            end_node = start_node + sliced_step
        else:
            end_node = start_node + (len(full_tweet_id_list) - end_node) - 1

        try:
            loop += 1
            is_tweepy = 1
            tweet_id_tweets_list = []
            total_attempt = 0
            start_time = time.time()
            while (total_attempt < len(CONSUMER_KEY_LIST)):
                request_api = tweepy_api_setup(CONSUMER_KEY_LIST[total_attempt], CONSUMER_SECRET_LIST[total_attempt],
                                               ACCESS_TOKEN_LIST[total_attempt],
                                               ACCESS_TOKEN_SECRET_LIST[total_attempt])
                tweet_id_tweets_list = tweepy_api_get_tweets_status(request_api, sliced_tweets_id_list)
                write_json_in_file(file_name + '_tweets_list.json', tweet_id_tweets_list)

                tweet_dict = {}
                for tweets_data in tweet_id_tweets_list:
                    csvwriter.writerow(
                        [tweets_data.id_str, str(tweets_data.user.id), str(tweets_data.text).strip('\t\n\r')])
                    # write_line_in_file(file_name+'_tweets_list.csv', 'a+',str(tweets_data.id_str) + ';' + str(tweets_data.user.id) +';'+ str(tweets_data.text).strip('\t\n\r') +';\n')

                print(tweet_dict)
                logging.info('attempt: ' + str(total_attempt))
                if len(tweet_id_tweets_list) != 0:
                    logging.info('success attempt: ' + str(total_attempt))
                    break;
                else:
                    total_attempt += 1

                if len(tweet_id_tweets_list) == 0 and total_attempt == len(CONSUMER_KEY_LIST):
                    total_attempt = 0
                    scrap_time = time.time() - start_time
                    next_api_time = sleeping_time - scrap_time
                    logging.info('sleeping the extracting (in sec): ' + str(sleeping_time))
                    time.sleep(sleeping_time)

        except Exception as e:
            logging.error(e)
            logging.info('loop : ' + str(loop))
            pass
    csvfile.close()


def score_readibility():
    csv_data = pd.read_csv(EXTRACTED_FILE_NAME + '.csv', delimiter=',')
    csv_data['flesh_reading_ease'] = 0
    csv_data['smog_index'] = 0
    csv_data['coleman_liau_index'] = 0
    csv_data['automated_readability_index'] = 0
    csv_data['dale_chall_readability_score'] = 0
    csv_data['linsear_write_formula'] = 0
    csv_data['gunning_fog'] = 0
    csv_data['language'] = ""
    for key, val in csv_data.get('tweet').items():
        val = strip_all_entities(remove_urls(val,''))
        csv_data.at[key, 'sentiment'] = tweet_ploarity_scoring(val)['compound']
        csv_data.at[key, 'tweet'] = val
        csv_data.at[key, 'flesh_reading_ease'] = textstat.flesch_reading_ease(val)
        csv_data.at[key, 'smog_index'] = textstat.smog_index(val)
        csv_data.at[key, 'coleman_liau_index'] = textstat.coleman_liau_index(val)
        csv_data.at[key, 'automated_readability_index'] = textstat.automated_readability_index(val)
        csv_data.at[key, 'dale_chall_readability_score'] = textstat.dale_chall_readability_score(val)
        csv_data.at[key, 'linsear_write_formula'] = textstat.linsear_write_formula(val)
        csv_data.at[key, 'gunning_fog'] = textstat.gunning_fog(val)
        # csv_data.at[key,'text_standard'] =  textstat.text_standard(val)
        try:
            lang = detect(val)
            if lang == 'en':
                lang_val = detect_langs(val)
                csv_data.at[key, 'language'] = detect_langs(val)[0].prob
            else:
                csv_data.at[key, 'language'] =  detect_langs(val)[0].lang+": "+detect_langs(val)[0].prob
        except:
            csv_data.at[key, 'language'] = ""

    csv_data.to_csv(EXTRACTED_FILE_NAME + '_sent_readability.csv', sep=',', encoding='utf-8')



