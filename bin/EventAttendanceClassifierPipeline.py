import numpy as np
from sklearn.model_selection import ParameterGrid
from sklearn.pipeline import Pipeline, FeatureUnion, make_pipeline
from NetworkEmbeddingFeature import NetworkEmbeddingFeature
from PreprocessTweetsExtractor import PreprocessTweetsExtractor
from DFFeatureExtractor import DFFeatureExtractor
from sklearn.svm import SVC

import pandas as pd
import random
from sklearn import model_selection, preprocessing
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from scipy.sparse import csr_matrix, hstack

from sklearn.model_selection import cross_val_score, cross_validate, train_test_split, cross_val_predict, KFold
import pickle
from pathlib import Path
from data_import import write_dict_to_csv, import_tweet_datafram, data_read_pickle_file, data_write_pickle_file, \
    is_file_exists, joblib_data_write, joblib_data_read
from time import time

from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.model_selection import cross_validate, train_test_split, cross_val_predict, KFold
from sklearn.pipeline import Pipeline
from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import Normalizer

from skorch import NeuralNetClassifier
from util.FNN import FNN
import torch
from torch.autograd import Variable
import torch.nn as nn

from yellowbrick.classifier import ROCAUC, PrecisionRecallCurve, ConfusionMatrix, ClassificationReport, \
    ClassPredictionError, DiscriminationThreshold
from util.plot.ROCCustom import ROCCustom
from util.plot.PRCustom import PRCustom


class EventAttendanceClassifierPipeline:
    def __init__(self, event_name, word_embd_list, net_embed_list, k_fold_split=3, net_embed_list_start=0,
                 net_embed_list_end=None, indicator_name=None):
        self.net_embed_list = net_embed_list
        self.word_embed_list = word_embd_list
        self.event_name = event_name
        self.cv_k_fold = KFold(n_splits=k_fold_split, shuffle=True)
        self.net_embed_list_start = net_embed_list_start
        self.net_embed_list_end = net_embed_list_end

        if indicator_name is not None:
            self.indicator_name = '_' + indicator_name
        else:
            self.indicator_name = ''

        self.feature_X = None
        self.feature_Y = None
        self.event_word_embed_name = None
        self.event_word_net_embed_name = None
        self.event_word_net_classfier_name = None
        self.color_label_0 = ['#023BD2', '#01B8FD', '#00C00F', '#DECA00', '#E76900', '#F20000']
        self.color_label_1 = ['#748FD6', '#8ADFFF', '#73D67A', '#E9DE6B', '#F5B784', '#FD9393']
        self.create_pipeline()

    def create_pipeline(self):
        event_name = self.event_name
        net_embedd_list = self.net_embed_list

        # import tweet into dataframe with sentiment and readability score
        event_tweet_feature_df, feature_y = import_tweet_datafram(event_name)
        self.feature_X = event_tweet_feature_df
        self.feature_Y = feature_y
        # get all word embedding list (BoW,TF-IDF)
        word_embed_pipeline_list = self.get_word_embed_list(self.word_embed_list)

        # traverse all network embedded methods
        for net_embed in net_embedd_list:
            net_embed_param_gid_list = ParameterGrid(self.get_net_embed_parameter(net_embed, event_name + '.edgelist'))
            print("network embed: " + net_embed + "\nparameter list length: " + str(len(net_embed_param_gid_list)))

            net_embed_param_list = list(net_embed_param_gid_list)

            if self.net_embed_list_end is None or self.net_embed_list_end > len(net_embed_param_list):
                self.net_embed_list_end = len(net_embed_param_list)
            if self.net_embed_list_start is None:
                self.net_embed_list_start = 0

            net_embed_param_list = net_embed_param_list[self.net_embed_list_start: self.net_embed_list_end]
            # traverse all network embedded PARAMETERS
            for net_embed_param in net_embed_param_list:
                list_index = list(net_embed_param_gid_list).index(net_embed_param)
                net_embed_param['id'] = list_index
                for word_embed in word_embed_pipeline_list:
                    self.word_embed_name = type(word_embed.steps[1][1]).__name__
                    self.event_word_net_embed_name = event_name + '_' + self.word_embed_name + '_' + net_embed_param[
                        'model'] + self.indicator_name
                    exist_embd_file = self.event_word_net_embed_name + '_' + str(list_index)
                    if is_file_exists('output/embed_model', exist_embd_file, 'p'):
                        f_embed = data_read_pickle_file('output/embed_model', exist_embd_file, 'p')
                    else:
                        f_embed = self.pipeline_word_net_union_embed(word_embed, net_embed_param,
                                                                     list_index)
                    self.run_classifiers(self.event_word_net_embed_name, list_index, f_embed, self.feature_Y)

    def pipeline_word_net_union_embed(self, word_embed_pipeline, net_embd_parameter,
                                      net_embd_param_indx):
        t1 = time()

        feature_text = word_embed_pipeline
        net_embd_param = net_embd_parameter.copy()
        # Create the pipeline for the other variables and add selection to choose features
        feature_sentiment = Pipeline([('sentiment', DFFeatureExtractor(columns=['sentiment']))])
        feature_readibility = Pipeline([('readibility', DFFeatureExtractor(columns=['readability'])),
                                        ('read_norm', Normalizer())])
        feature_net_embed = Pipeline([('net_embed', NetworkEmbeddingFeature(**net_embd_param))])
        # Merge pipelines using FeatureUnion
        features_concate = FeatureUnion(transformer_list=[('f_text', feature_text),
                                                          ('f_sentiment', feature_sentiment),
                                                          ('f_readibility', feature_readibility),
                                                          ('f_network', feature_net_embed)])
        clf_pipeline = Pipeline([('f_concate', features_concate),
                                 ('clf', SVC(C=10, kernel='linear', gamma=.001))])

        # word embedding model
        f_word_embed = features_concate.transformer_list[0][1].fit_transform(self.feature_X, self.feature_Y)
        f_word_embed_params = features_concate.transformer_list[0][1].get_params()
        f_net_embed = features_concate.transformer_list[2][1].fit_transform(self.feature_X, self.feature_Y)
        f_net_embed_params = features_concate.transformer_list[2][1].get_params()
        f_net_embed_params['index'] = net_embd_param_indx

        # concate model params
        f_embedd = features_concate.fit_transform(self.feature_X, self.feature_Y)
        net_embd_param['train_time'] = time() - t1

        write_dict_to_csv('output/word_embed_model', self.event_word_net_embed_name, f_word_embed_params)
        data_write_pickle_file(f_word_embed, 'output/word_embed_model', self.event_word_net_embed_name, 'p')

        write_dict_to_csv('output/net_embed_model', self.event_word_net_embed_name, f_net_embed_params)
        data_write_pickle_file(f_net_embed, 'output/net_embed_model',
                               self.event_word_net_embed_name + '_' + str(net_embd_param_indx),
                               'p')

        write_dict_to_csv('output/embed_model', self.event_word_net_embed_name, net_embd_param)
        data_write_pickle_file(f_embedd, 'output/embed_model',
                               self.event_word_net_embed_name + '_' + str(net_embd_param_indx), 'p')
        return f_embedd

    def run_classifiers(self, feature_name, net_index, feature_matrix_X=None, feature_matrix_Y=None):

        fnn_net = NeuralNetClassifier(
            module=FNN,
            module__input_size=feature_matrix_X.shape[1],
            module__hidden_size=int((feature_matrix_X.shape[1] + 2) / 2),
            module__num_classes=2,
            criterion=nn.CrossEntropyLoss,
            optimizer=torch.optim.Adam,
            optimizer__lr=0.01,
            warm_start=True,
            max_epochs=20
        )
        # net_embed_param_gid_list = ParameterGrid(get_classifier_parameter())
        clfs = []

        clfs.append(LogisticRegression())
        clfs.append(SVC(C=10, kernel='linear', gamma=.001, probability=True))
        # clfs.append(SVC(C=10, kernel='rbf', gamma=.001, probability=True))
        clfs.append(DecisionTreeClassifier())
        clfs.append(RandomForestClassifier())
        clfs.append(GradientBoostingClassifier())
        clfs.append(fnn_net)
        print("classifiers")
        scoring = ['accuracy', 'average_precision', 'f1', 'f1_macro', 'f1_micro', 'neg_log_loss',
                   'neg_mean_squared_error',
                   'precision',
                   'precision_macro',
                   'precision_micro', 'r2', 'recall', 'recall_macro',
                   'recall_micro', 'roc_auc']
        # scoring = ['accuracy']
        index = 0
        for classifier in clfs:
            is_file_saved = False
            is_neural_net = False
            classifier_name = type(classifier).__name__
            clf_param = classifier.get_params()
            # convert the features into float type for only neural network
            if (classifier_name.lower() == 'neuralnetclassifier'):
                is_neural_net = True
                classifier_name = classifier.module.__name__
                feature_matrix_X = feature_matrix_X.astype(np.float32)
                feature_matrix_Y = feature_matrix_Y.astype(np.int64)

            self.event_word_net_classfier_name = self.event_word_net_embed_name + "_" + str(
                net_index) + "_" + classifier_name
            if not is_neural_net:
                if not is_file_exists('output/classifier_model', self.event_word_net_classfier_name, 'model'):
                    is_file_saved = False
                    classifier.fit(feature_matrix_X, feature_matrix_Y)
                    joblib_data_write('output/classifier_model', self.event_word_net_classfier_name, 'model',
                                      classifier)

                else:
                    is_file_saved = True
                    classifier = joblib_data_read('output/classifier_model', self.event_word_net_classfier_name,
                                                  'model')

            else:
                param_path = str(
                    Path('../output/classifier_model/' + self.event_word_net_classfier_name + '_param.pkl'))
                opt_path = str(Path('../output/classifier_model/' + self.event_word_net_classfier_name + '_opt.pkl'))
                history_path = str(
                    Path('../output/classifier_model/' + self.event_word_net_classfier_name + '_history.json'))
                # if not is_file_exists('output/classifier_model', self.event_word_net_classfier_name+ '_param', 'pkl'):
                #     is_file_saved = False
                #     classifier.fit(feature_matrix_X, feature_matrix_Y)
                #     classifier.save_params(f_params=param_path, f_optimizer=opt_path, f_history=history_path)
                # else:
                #     is_file_saved = True
                #     classifier.initialize()
                #     classifier.load_params(f_params=param_path,f_history=history_path,f_optimizer=opt_path)
                #     classifier.fit(feature_matrix_X, feature_matrix_Y)

            clf_scores = cross_validate(classifier, feature_matrix_X, feature_matrix_Y, scoring=scoring,
                                        cv=self.cv_k_fold)

            print(str(net_index))
            print('---------------------------------')
            print(str(classifier))
            print('-----------------------------------')
            clf_score_std_mean = {}
            for key, values in clf_scores.items():
                clf_score_std_mean[key + '_mean '] = values.mean()
                clf_score_std_mean[key + '_std '] = values.std()
                print(key, ' mean ', values.mean())
                print(key, ' std ', values.std())

            clf_scores['emb_model'] = feature_name
            clf_scores['emb_index'] = index
            clf_scores['saved_model'] = is_file_saved
            clf_score_std_mean['emb_model'] = feature_name
            clf_score_std_mean['emb_index'] = net_index
            clf_score_std_mean['saved_model'] = is_file_saved

            clf_score_std_mean['clf_name'] = classifier_name
            write_dict_to_csv('output/classifier_param', feature_name, clf_param)
            write_dict_to_csv('output/classifier_model', feature_name + '_mean_std', clf_score_std_mean)
            # try:
            #     self.generate_visualize(classifier, feature_matrix_X, feature_matrix_Y, index=index)
            # except Exception as e:
            #     print(e)
            #     pass

            index += 1

        print('generate roc and pr curve')
        try:
            self.generate_roc_curve_visualize(clfs, feature_matrix_X, feature_matrix_Y,
                                              self.event_word_net_embed_name + '_' + str(net_index), clear_fig=False)
            self.generate_pr_curve_visualize(clfs, feature_matrix_X, feature_matrix_Y,
                                             self.event_word_net_embed_name + '_' + str(net_index),
                                             clear_fig=False)
        except Exception as e:
            print('roc curve pr curve error')
            pass

    def generate_visualize(self, classifier_model, f_x, f_y, clear_figure=True, index=0):
        # TODO: visualize prediction
        for train_index, test_index in self.cv_k_fold.split(f_x):
            X_train, X_test = f_x[train_index], f_x[test_index]
            y_train, y_test = f_y[train_index], f_y[test_index]
        classes = [1, 0]
        visualizer_list = []
        # ROC Curve
        roc_visualizer = ROCCustom(classifier_model, classes=classes,
                                colors=[self.color_label_0[index], self.color_label_1[index]],
                                   per_class=True, macro=False, micro=False)
        visualizer_list.append(roc_visualizer)
        roc_visualizer.fit(X_train, y_train)  # Fit the training data to the visualizer
        roc_visualizer.score(X_test, y_test)  # Evaluate the model on the test data
        roc_visualizer.poof(clear_figure=clear_figure,
                            outpath=str(Path('../output/plot/roc/' + self.event_word_net_classfier_name + '.png')))

        # Precision Recall curve
        pr_visualizer = PrecisionRecallCurve(classifier_model, classes=classes,
                                             colors=[self.color_label_0[index], self.color_label_1[index]],
                                             per_class=True, iso_f1_curves=True,
                                             fill_area=False, micro=False)
        visualizer_list.append(pr_visualizer)
        pr_visualizer.fit(X_train, y_train)  # Fit the training data to the visualizer
        pr_visualizer.score(X_test, y_test)  # Evaluate the model on the test data
        pr_visualizer.poof(clear_figure=clear_figure,
                           outpath=str(Path('../output/plot/pr_curve/' + self.event_word_net_classfier_name + '.png')))

        # # Confusion Matrix curve
        # conf_mat_visualizer = ConfusionMatrix(classifier_model, classes=classes)
        # visualizer_list.append(conf_mat_visualizer)
        # conf_mat_visualizer.fit(X_train, y_train)  # Fit the training data to the visualizer
        # conf_mat_visualizer.score(X_test, y_test)  # Evaluate the model on the test data
        # conf_mat_visualizer.poof(clear_figure=True,
        #                          outpath=str(Path(
        #                              '../output/plot/confusion_matrix/' + self.event_word_net_classfier_name + '.png')))
        # 
        # # Classificaion report curve
        # class_report_visualizer = ClassificationReport(classifier_model, classes=classes, support=True)
        # visualizer_list.append(class_report_visualizer)
        # class_report_visualizer.fit(X_train, y_train)  # Fit the training data to the visualizer
        # class_report_visualizer.score(X_test, y_test)  # Evaluate the model on the test data
        # class_report_visualizer.poof(clear_figure=True,
        #                              outpath=str(
        #                                  Path(
        #                                      '../output/plot/classifier_report/' + self.event_word_net_classfier_name + '.png')))
        # 
        # # Classificaion prediction curve
        # class_pred_visualizer = ClassPredictionError(classifier_model, classes=classes)
        # visualizer_list.append(class_pred_visualizer)
        # class_pred_visualizer.fit(X_train, y_train)  # Fit the training data to the visualizer
        # class_pred_visualizer.score(X_test, y_test)  # Evaluate the model on the test data
        # class_pred_visualizer.poof(clear_figure=True,
        #                            outpath=str(
        #                                Path(
        #                                    '../output/plot/classifier_pred_error/' + self.event_word_net_classfier_name + '.png')))

        # Discrimination Threshold curve
        # visualizer = DiscriminationThreshold(classifier_model)
        # visualizer_list.append(visualizer)
        # visualizer.fit(X_train, y_train)  # Fit the training data to the visualizer
        # visualizer.score(X_test, y_test)  # Evaluate the model on the test data
        # visualizer.poof(clear_figure=True,
        #                 outpath=str(
        #                     Path(
        #                         '../output/plot/discrimination_threshold/' + self.event_word_net_classfier_name + '.png')))
        # return visualizer_list

    def generate_roc_curve_visualize(self, clf_list, f_x, f_y, file_name, clear_fig=True):
        # TODO: visualize prediction
        for train_index, test_index in self.cv_k_fold.split(f_x):
            X_train, X_test = f_x[train_index], f_x[test_index]
            y_train, y_test = f_y[train_index], f_y[test_index]
        classes = [1, 0]
        # ROC Curve
        index = 0
        clf_name = ''
        for clf in clf_list:
            clf_name = clf_name + ' \n ' + type(clf).__name__
            print(clf_name)
            if (type(clf).__name__.lower().strip() == 'neuralnetclassifier'):
                print("neural net data cocnversion")
                X_train = X_train.astype(np.float32)
                y_train = y_train.astype(np.int64)

                X_test = X_test.astype(np.float32)
                y_test = y_test.astype(np.int64)

            if index == len(clf_list) - 1:
                clear_fig = True
            else:
                clear_fig = clear_fig

            visualizer = ROCCustom(clf, classes=classes,
                                colors=[self.color_label_0[index], self.color_label_1[index]],
                                title='ROC Curve for event attendance prediction',per_class=True, macro=False, micro=False)
            visualizer.fit(X_train, y_train)  # Fit the training data to the visualizer
            visualizer.score(X_test, y_test)  # Evaluate the model on the test data
            visualizer.poof(clear_figure=clear_fig,
                            outpath=str(Path('../output/plot/roc/combined/' + file_name + '.png')))
            index += 1

    def generate_pr_curve_visualize(self, clf_list, f_x, f_y, file_name, clear_fig=True):
        # TODO: visualize prediction
        for train_index, test_index in self.cv_k_fold.split(f_x):
            X_train, X_test = f_x[train_index], f_x[test_index]
            y_train, y_test = f_y[train_index], f_y[test_index]
        classes = [1, 0]
        index = 0
        clf_name = ''
        for clf in clf_list:
            clf_name = clf_name + ' \n ' + type(clf).__name__
            print(clf_name)
            if (type(clf).__name__.lower().strip() == 'neuralnetclassifier'):
                print("neural net data cocnversion")
                X_train = X_train.astype(np.float32)
                y_train = y_train.astype(np.int64)

                X_test = X_test.astype(np.float32)
                y_test = y_test.astype(np.int64)

            if index == len(clf_list) - 1:
                clear_fig = True
            else:
                clear_fig = clear_fig
            visualizer = PrecisionRecallCurve(clf, classes=classes, per_class=True, iso_f1_curves=True,
                                              fill_area=False, micro=False,
                                              colors=[self.color_label_0[index], self.color_label_1[index]],
                                              title='PR Curve for event attendance')
            visualizer.fit(X_train, y_train)  # Fit the training data to the visualizer
            visualizer.score(X_test, y_test)  # Evaluate the model on the test data
            visualizer.poof(clear_figure=clear_fig,
                            outpath=str(Path('../output/plot/pr_curve/combined/' + file_name + '.png')))
            index += 1


    def get_classifier_parameter(self, clf_name):
        clf_param = {}
        # SVM classifier
        if clf_name == 'svc':
            clf_param = {}
        # Random Forest Classifier
        elif clf_name == 'rf':
            clf_param = {}
        # logistic regression
        elif clf_name == 'lr':
            clf_param = {}
        # Decision Tree
        elif clf_name == 'dt':
            clf_param = {}
        # Gradient Boost classifier
        elif clf_name == 'gb':
            clf_param = {}

        return clf_param

    def get_net_embed_parameter(self, net_embedd_name, file_path):
        embedding_param = {}
        if net_embedd_name == 'node2vec':
            embedding_param = {'model': [net_embedd_name],
                               'file_name': [file_path],
                               'dimension': [4, 64, 72, 128],
                               'window_size': [300],
                               'min_count': [3],
                               'n2v_batch_words': [4],
                               'walk_len': [30],
                               'num_walks': [15],
                               'con_size': [10],
                               'ret_p': [0.1],
                               'inout_q': [0.1]}
        elif net_embedd_name == 'harp':
            embedding_param = {'model': [net_embedd_name],
                               'dimension': [4, 64, 72, 128],
                               'harp_model': ['node2vec'],
                               'file_name': [file_path],
                               'num_walks': [15],
                               'walk_len': [30],
                               'window_size': [30],
                               'workers': [10]}

        elif net_embedd_name == 'hope':
            embedding_param = {'model': [net_embedd_name],
                               'file_name': [file_path],
                               'dimension': [4, 64, 72, 128],
                               'beta': [0.1]}
        elif net_embedd_name == 'sdne':
            embedding_param = {'model': [net_embedd_name],
                               'file_name': [file_path],
                               'dimension': [4, 64, 72, 128],
                               'beta': [5],
                               'sdne_K': [3],
                               'sdne_n_units': [[50, 15, ]],
                               'sdne_n_iter': [20],
                               'sdne_xeta': [0.01],
                               'sdne_n_batch': [200]}

        elif net_embedd_name == 'walklet':
            embedding_param = {'model': [net_embedd_name],
                               'file_name': [file_path],
                               'dimension': [4, 64, 72, 128],
                               'window_size': [30],
                               'min_count': [10],
                               'workers': [10],
                               'walklet_type': ["first", "other"],
                               'walk_len': [30],
                               'num_walks': [15],
                               'ret_p': [0.1],
                               'inout_q': [0.1]}
        elif net_embedd_name == 'prune':
            embedding_param = {'model': [net_embedd_name],
                               'file_name': [file_path],
                               'dimension': [4, 64, 72, 128],
                               'prune_lamb': [0.01],
                               'prune_learning_rate': [1e-4],
                               'prune_epoch': [100],
                               'prune_gpu_fraction': [0.2],
                               'prune_batch_size': [100]}

        return embedding_param

    def get_word_embed_list(self, word_embed_list):
        tf_idf_pipeline = Pipeline([('process_tweets', PreprocessTweetsExtractor('text')),
                                    ('word_embed',
                                     TfidfVectorizer(analyzer='word', token_pattern=r'\w{1,}', ngram_range=(2, 3),
                                                     stop_words='english'))])
        bow_pipeline = Pipeline([('process_tweets', PreprocessTweetsExtractor('text')),
                                 (
                                     'word_embed',
                                     CountVectorizer(analyzer='word', token_pattern=r'\w{1,}', ngram_range=(2, 3),
                                                     stop_words='english'))])
        word_embed_pipeline_list = []
        for word_embed in word_embed_list:
            if word_embed == 'bow':
                word_embed_pipeline_list.append(bow_pipeline)
            elif word_embed == 'tfidf':
                word_embed_pipeline_list.append(tf_idf_pipeline)

        return word_embed_pipeline_list
