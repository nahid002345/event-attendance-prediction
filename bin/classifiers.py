from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.model_selection import cross_validate,train_test_split,cross_val_predict, KFold
from sklearn.pipeline import Pipeline
from sklearn.metrics import confusion_matrix

def run_classifiers(feature_matrix_X=None, feature_matrix_Y=None):
    cv = KFold(n_splits=3, shuffle = True)
    # X_train, valid_x, y_train, valid_y = train_test_split(feature_matrix_X, feature_matrix_Y)
    # pipeline = Pipeline([('clf', LogisticRegression())])
    clfs = []
    # clfs.append(LogisticRegression())
    clfs.append(SVC(C= 10, kernel='linear', gamma=.001))
    clfs.append(SVC(C=10, kernel='rbf', gamma=.001))
    # clfs.append(DecisionTreeClassifier())
    # clfs.append(RandomForestClassifier())
    # clfs.append(GradientBoostingClassifier())
    print("classifiers")
    scoring = ['precision', 'recall', 'f1', 'accuracy']
    for classifier in clfs:
        # pipeline.set_params(clf=classifier)
        y_pred = cross_val_predict(classifier, feature_matrix_X, feature_matrix_Y, cv=cv)
        print(y_pred)
        print(feature_matrix_Y)
        conf_mat = confusion_matrix(feature_matrix_Y, y_pred)
        print(conf_mat.ravel())
        print(conf_mat)
        scores = cross_validate(classifier, feature_matrix_X, feature_matrix_Y, scoring=scoring,cv=cv)
        print('---------------------------------')
        print(str(classifier))
        print('-----------------------------------')
        for key, values in scores.items():
            print(key, ' mean ', values.mean())
            print(key, ' std ', values.std())
