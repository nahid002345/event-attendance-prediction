import matplotlib.pyplot as plt

from graph.util import graph_util, plot_util
from graph.evaluation import visualize_embedding as viz
from graph.evaluation import evaluate_graph_reconstruction as gr
from time import time

# from graph.embedding.node2vec import node2vec
from graph.embedding.gf import GraphFactorization
from graph.embedding.hope import HOPE
from graph.embedding.walklets.walklets import WalkletMachine
from graph.embedding.sdne import SDNE
from graph.embedding.node2vec2 import node2vec2
from graph.embedding.prune import PRUNE
from graph.embedding.harp.harp2 import HARP
from node2vec import Node2Vec
from pathlib import Path
from NetworkEmbeddingFeature import NetworkEmbeddingFeature
from sklearn.pipeline import Pipeline


def get_graph_from_edge_list(edge_list_file):
    # File that contains the edges. Format: source target
    # Optionally, you can add weights as third column: source target weight
    edge_f = edge_list_file
    # Specify whether the edges are directed
    isDirected = True

    # Load graph
    G = graph_util.loadGraphFromEdgeListTxt(edge_f, directed=isDirected)
    G = G.to_directed()
    # net_embed = Pipeline([('net_embed', NetworkEmbeddingFeature(model='node2vec',file_name='anthrocon.edgelist',))])
    # graph = net_embed.fit_transform('anthrocon.edgelist')
    models = []
    # You can comment out the methods you don't want to run
    # GF takes embedding dimension (d), maximum iterations (max_iter), learning rate (eta), regularization coefficient (regu) as inputs
    # models.append(GraphFactorization(d=2, max_iter=100000, eta=1 * 10 ** -4, regu=1.0))
    # HOPE takes embedding dimension (d) and decay factor (beta) as inputs
    # models.append(HOPE(d=4, beta=0.01))
    # walklets takes embedding dimension (d) & other parameters
    # models.append(WalkletMachine(graph=G, dimensions=4))
    # PRUNE takes proximity preserving network embedding
    # models.append(PRUNE(scope_name='default', dimension=4, lamb=0.01, learning_rate=1e-4, epoch=10, gpu_fraction=0.2,
    #                     batchsize=332,
    #                     print_every_epoch=1, save_checkpoints=False))
    # HARP process
    # models.append(HARP(format='edgelist', input=Path('../dataset/graph.edgelist'), model='node2vec')))
    # # LE takes embedding dimension (d) as input
    # models.append(LaplacianEigenmaps(d=2))
    # # LLE takes embedding dimension (d) as input
    # models.append(LocallyLinearEmbedding(d=2))
    # node2vec takes embedding dimension (d),  maximum iterations (max_iter), random walk length (walk_len), number of random walks (num_walks), context size (con_size), return weight (ret_p), inout weight (inout_p) as inputs
    models.append(
        node2vec2(d=128, window_size=10, min_count=1, batch_words=4, max_iter=1, walk_len=80, num_walks=10, con_size=10,
                  ret_p=1, inout_p=1))
    # SDNE takes embedding dimension (d), seen edge reconstruction weight (beta), first order proximity weight (alpha), lasso regularization coefficient (nu1), ridge regreesion coefficient (nu2), number of hidden layers (K), size of each layer (n_units), number of iterations (n_ite), learning rate (xeta), size of batch (n_batch), location of modelfile and weightfile save (modelfile and weightfile) as inputs
    # models.append(
        # SDNE(d=4, beta=5, alpha=1e-5, nu1=1e-6, nu2=1e-6, K=3, n_units=[50, 15, ], n_iter=50, xeta=0.01, n_batch=500))

    for embedding in models:
        print('Num nodes: %d, num edges: %d' % (G.number_of_nodes(), G.number_of_edges()))
        t1 = time()
        # Learn embedding - accepts a networkx graph or file with edge list
        Y, t = embedding.learn_embedding(graph=G, edge_f=None, is_weighted=False, no_python=True)
        print(':\n\tTraining time: %f' % (time() - t1))
        # Evaluate on graph reconstruction
        # MAP, prec_curv, err, err_baseline = gr.evaluateStaticGraphReconstruction(G, embedding, Y, None)
        # # ---------------------------------------------------------------------------------
        # print(("\tMAP: {} \t precision curve: {}\n\n\n\n" + '-' * 100).format(MAP, prec_curv[:5]))
        # ---------------------------------------------------------------------------------

        mat_embed = embedding.get_embedding()
        shp = mat_embed.shape
        print(shp)
        # Visualize
        # viz.plot_embedding2D(embedding.get_embedding(),di_graph=G,dim_reduce='pca')
        # plt.show()

    return mat_embed
