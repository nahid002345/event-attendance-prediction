import numpy
import os.path
import csv
import requests
from bs4 import BeautifulSoup
from datetime import datetime
import re
import pandas as pd
import numpy as np
import json
from pathlib import Path
import pickle
import joblib
# from util.text_processing import text_processed

cur_path = os.path.dirname(__file__)
new_dataset_path = os.path.relpath('..\\dataset', cur_path)
new_common_path = os.path.relpath('..', cur_path)


# def is_file_exists(file_name):
#     is_exists = False
#     is_exists = os.path.isfile(new_dataset_path + '\\' + file_name)
#     return is_exists

def joblib_data_write(file_directory,file_name,file_extension,model):
    file_path = Path('../' + file_directory + '/' + file_name + '.' + file_extension)
    joblib.dump(model,file_path)

def joblib_data_read(file_directory,file_name,file_extension):
    file_path = Path('../' + file_directory + '/' + file_name + '.' + file_extension)
    model = joblib.load(file_path)
    return model

def is_file_exists(file_directory,file_name,file_extension):
    is_exists = False
    file_path = Path('../' + file_directory + '/' + file_name + '.'+file_extension)
    is_exists = os.path.isfile(file_path)
    return is_exists

def data_read_pickle_file(file_directory,file_name,file_extension='pkl'):
    file_path = Path('../' + file_directory + '/' + file_name + '.'+file_extension)
    with open(file_path, 'rb') as file:
        loaded_data = pickle.load(file)
        return loaded_data


def data_write_pickle_file(data,file_directory,file_name,file_extension='pkl'):
    file_path = Path('../' + file_directory + '/' + file_name + '.'+file_extension)
    with open(file_path, 'wb') as file:
        pickle.dump(data,file)

def data_import_pickle_panda(file_directory,file_name):
    return pandas.read_pickle(Path('../' + file_directory + '/' + file_name + '.pkl'))


def data_export_pandas_pickle(dataframe, file_directory,file_name):
    dataframe.to_pickle(Path('../' + file_directory + '/' + file_name + '.pkl'))


def data_txt_import_string(file_name):
    file_path = Path('../dataset/' + file_name)
    file = open(file_path, 'r', encoding='utf-8')
    data = file.readline()
    return data


def data_txt_import_array(file_name):
    file_path = Path('../dataset/' + file_name)
    file = open(file_path, 'r', encoding='utf-8')
    list = []
    for line in file:
        list.append(line)

    data = ''.join(list)
    return data.lower()


def data_txt_import_list(file_name, delimiter='\t'):
    import_list = []
    try:
        with open(file_name, 'r', encoding='utf-8') as file:
            data = file.readlines()
            for line in data:
                data_id = line.replace(" ", "\t").rstrip("\n\r").split(delimiter)[0];
                if data_id.isdigit():
                    import_list.append(re.split(delimiter, line.replace(" ", "\t").rstrip("\n\r")))
    except Exception as e:
        print(e)
        pass
    return import_list


def data_csv_import(file_name):
    file_path = Path('../dataset/' + file_name)
    with open(file_path) as csvfile:
        readCSV = csv.reader(csvfile)
        csv_col_data = []
        for row in readCSV:
            # check for exact duplicate entry
            if row[4].strip() not in csv_col_data:
                csv_col_data.append(row[4].strip() + ".")
    data = ''.join(csv_col_data)
    return data

def data_csv_import(file_name,data_column_index=0):
    file_path = Path('../dataset/' + file_name)
    with open(file_path) as csvfile:
        readCSV = csv.reader(csvfile)
        csv_col_data = []
        for row in readCSV:
            # check for exact duplicate entry
            if row[data_column_index].strip() not in csv_col_data:
                csv_col_data.append(row[data_column_index].strip() + ".")
    data = ''.join(csv_col_data)
    return data


def data_csv_import_array(file_name, delimiter=';'):
    file_path = Path('../dataset/cleaned/' + file_name)
    with open(file_path, encoding='utf-8', errors='ignore') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=delimiter,
                             quoting=csv.QUOTE_ALL)
        csv_col_data = []
        for row in readCSV:
            if row[0]:
                csv_col_data.append(row)

    return csv_col_data


def data_csv_import_array_by_col_index(file_name, col_index, delimiter=','):
    with open(file_name, encoding='utf-8') as csvfile:
        readCSV = csv.reader(csvfile, quotechar='"', delimiter=delimiter,
                             quoting=csv.QUOTE_ALL)
        csv_col_data = []
        next(readCSV, None)
        for row in readCSV:
            if(len(row) > 0):
                csv_col_data.append(row[col_index])

    return csv_col_data


def data_csv_column_array(file_name, delimiter=',', colnames=[]):
    data = pandas.read_csv(file_name, encoding='utf-8', delimiter=delimiter, names=colnames)
    return data


def remove_duplicates(list):
    newlist = []
    for item in list:
        if item not in newlist:
            newlist.append(item)
    return newlist


def column_from_matrix(matrix, i):
    return [row[i] for row in matrix]


def web_scraping():
    NoneType = type(None)
    url = "http://www.gesetze-im-internet.de/englisch_abgg/englisch_abgg.html#p0333"
    r = requests.get(url)

    online_data = r.text
    data = []
    soup = BeautifulSoup(online_data, 'html.parser')
    list = soup.find_all('p')

    csv_directory_path = "H:\\University of Passau\\Projects\\Text Mining\\"
    date_time = datetime.now()
    filename = date_time.strftime('%Y%m%d%H%M%S') + '.csv'
    csv_file = open(csv_directory_path + filename, 'w', newline='')
    csv_writer = csv.writer(csv_file, )
    csv_header = ['file_name', 'id', 'class', 'tocase', 'text']
    csv_writer.writerow(csv_header)

    for p in list:
        data.clear()
        data.append('p')
        data.append('id')
        data.append('class')
        data.append('case')
        data.append(re.sub('[^A-Za-z0-9 ]+', '', p.text.rstrip()))
        if len(data) > 0:
            csv_writer.writerow(data)
        print(p.text)

    csv_file.close()


def convert_list_to_dict(data_list, key_col=0, value_col=1):
    list_dict = {}

    for index, key in enumerate(data_list):
        key_val = key[key_col]
        if key_val in list_dict.keys():
            list_dict[key_val].append(int(data_list[index][value_col]))
        else:
            list_dict[key_val] = []
            list_dict[key_val].append(int(data_list[index][value_col]))

    return list_dict


def convert_dict_to_list(data_dict):
    dict_list = []
    for key, value in data_dict.items():
        temp = [key, value]
        dict_list.append(temp)

    return dict_list


def write_line_in_file(file_name, file_read_type, wriiten_text):
    with open(file_name, file_read_type, encoding='utf-8') as write_file:
        write_file.write(wriiten_text)

def write_json_in_file(file_name, data_list):
    with open(file_name, 'a+') as outfile:
        for data in data_list:
            json.dump(data._json, outfile)

def write_csv_in_file(file_name,data_list):
    with open(file_name, 'a+', newline='',encoding='utf-8') as csv_file:
        wr = csv.writer(csv_file, quoting=csv.QUOTE_ALL)
        if isinstance(data_list,list):
            for data in data_list:
                wr.writerow(data)
        else:
            wr.writerow(data_list)


def write_dict_to_csv(file_directory, file_name,csv_data):
    csv_file = Path('../' + file_directory + '/' + file_name + '.csv')
    file_exist = is_file_exists(file_directory,file_name,'csv')
    csv_columns = list(csv_data.keys())
    try:
        with open(csv_file, 'a+') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
            if not file_exist:
                writer.writeheader()
                file_exist = True

            writer.writerow(csv_data)
            # for data in csv_data:
            #     writer.writerow(data)
    except IOError:
        print("I/O error")

def merge_tweet_label(row):
    value = 0
    row = row.tolist()
    row_count_one = row.count('1')
    row_count_zero = row.count('0')
    if row_count_one >= row_count_zero:
        value = 1
    else:
        value = 0
    return value;

def import_tweet_datafram(file_name="anthrocon"):
    DATASET_NAME = file_name+'_tweets'
    file_path = "output/embed_model"
    train_df = pd.DataFrame()
    # save panda dataframe to a pickle file so that load it easily and escape text processing part
    if not is_file_exists(file_path,DATASET_NAME, ".pkl"):
        data = data_csv_import_array(DATASET_NAME + ".csv",delimiter=',')
        user_data = column_from_matrix(data, 3)
        text_data = column_from_matrix(data, 4)
        label_data = column_from_matrix(data, 7)
        sentiment_data = np.array(column_from_matrix(data, 6))
        readability_data = np.array(column_from_matrix(data, 5))
        train_df['user'] = user_data
        # processed_text = text_processed(text_data)
        train_df['text'] = text_data
        train_df['label'] = label_data
        train_df['readability'] = readability_data
        train_df['sentiment'] = sentiment_data
        data_export_pandas_pickle(train_df,file_path, DATASET_NAME)
    else:
        train_df = data_import_pickle_panda(file_path, DATASET_NAME)

    ## combine duplicate user_node into a single row
    train_df = train_df.groupby('user').agg({
        'text': lambda x: ', '.join(x),
        'label': merge_tweet_label,
        'sentiment': lambda x: x.astype(np.float).mean(),
        'readability': lambda x: x.astype(np.float).mean()
    })

    return train_df,np.asarray(list(train_df['label']))
