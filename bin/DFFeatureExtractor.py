from sklearn.base import BaseEstimator, TransformerMixin
import pandas as pd
from scipy import sparse


class DFFeatureExtractor(BaseEstimator, TransformerMixin):
    def __init__(self, columns):
        self.columns = columns

    def fit(self, df, y=None):
        return self

    def transform(self, df, y=None):
        df_matrix = df.as_matrix(self.columns)
        return sparse.csr_matrix(df_matrix)
