from sklearn.base import BaseEstimator, TransformerMixin
from graph.util import graph_util, plot_util
from graph.evaluation import visualize_embedding as viz
from graph.evaluation import evaluate_graph_reconstruction as gr
from time import time

# from graph.embedding.node2vec import node2vec
from graph.embedding.gf import GraphFactorization
from graph.embedding.hope import HOPE
from graph.embedding.walklets.walklets import WalkletMachine
from graph.embedding.sdne import SDNE
from graph.embedding.node2vec2 import node2vec2
from graph.embedding.prune import PRUNE
from graph.embedding.harp.harp2 import HARP
from node2vec import Node2Vec
from pathlib import Path
from scipy import sparse
from graph.evaluation.visualize_embedding import VisualEmbedding
from data_import import write_dict_to_csv, import_tweet_datafram, data_read_pickle_file, data_write_pickle_file, \
    is_file_exists
from time import time
class NetworkEmbeddingFeature(BaseEstimator, TransformerMixin):
    def __init__(self,id=0, model='node2vec', file_name='',
                 dimension=128, window_size=5, min_count=1, beta=0.01, workers=1, walk_len=80, num_walks=10, ret_p=1.0,
                 inout_q=1.0,con_size=10,
                 n2v_batch_words=4, n2v_max_iteration=1,
                 sdne_alpha=1e-5, sdne_nu1=1e-6, sdne_nu2=1e-6, sdne_K=3, sdne_n_units=[50, 15, ], sdne_n_iter=50,
                 sdne_xeta=0.01, sdne_n_batch=500,
                 walklet_type="first", walklet_output=None,
                 prune_lamb=0.01, prune_learning_rate=1e-4, prune_epoch=10, prune_gpu_fraction=0.02,
                 prune_batch_size=500, prune_print_every_epoch=1, prune_save_checkpoints=False,
                 harp_format='edgelist', harp_model='node2vec', harp_represent_size=128,
                 harp_matfile_variable_name='network'):
        self.id = id
        self.model = model
        self.file_name = file_name
        self.dimension = dimension
        self.window_size = window_size
        self.min_count = min_count
        self.beta = beta
        self.workers = workers
        self.walk_len = walk_len
        self.num_walks = num_walks
        self.ret_p = ret_p
        self.inout_q = inout_q
        self.con_size = con_size
        self.n2v_batch_words = n2v_batch_words
        self.n2v_max_iteration = n2v_max_iteration
        self.sdne_alpha = sdne_alpha
        self.sdne_nu1 = sdne_nu1
        self.sdne_nu2 = sdne_nu2
        self.sdne_K = sdne_K
        self.sdne_n_units = sdne_n_units
        self.sdne_n_iter = sdne_n_iter
        self.sdne_xeta = sdne_xeta
        self.sdne_n_batch = sdne_n_batch
        self.walklet_type = walklet_type
        self.walklet_output = walklet_output
        self.prune_lamb = prune_lamb
        self.prune_learning_rate = prune_learning_rate
        self.prune_epoch = prune_epoch
        self.prune_gpu_fraction = prune_gpu_fraction
        self.prune_batch_size = prune_batch_size
        self.prune_print_every_epoch = prune_print_every_epoch
        self.prune_save_checkpoints = False
        self.harp_format = harp_format
        self.harp_model = harp_model
        self.harp_represent_size = harp_represent_size
        self.harp_matfile_variable_name = harp_matfile_variable_name

    def fit(self, X, y=None, **kwargs):
        return self

    def transform(self, X, y=None, **kwargs):
        file_path = Path('../dataset/cleaned/' + self.file_name)

        # t1 = time()
        G = graph_util.loadGraphFromEdgeListTxt(file_path, directed=True)
        G = G.to_directed()
        print('Num nodes: %d, num edges: %d' % (G.number_of_nodes(), G.number_of_edges()))
        if self.model == 'node2vec':
            self.embedding = node2vec2(d=self.dimension, window_size=self.window_size, min_count=self.min_count,
                                  batch_words=self.n2v_batch_words, max_iter=self.n2v_max_iteration,
                                  walk_len=self.walk_len, num_walks=self.num_walks, con_size=self.con_size,
                                  ret_p=self.ret_p, inout_p=self.inout_q)
        elif self.model == 'harp':
            self.embedding = HARP(representation_size=self.dimension, walk_length=self.walk_len,
                             window_size=self.window_size, workers=self.workers, number_walks=self.num_walks,
                             format=self.harp_format, input=file_path, model=self.harp_model)
        elif self.model == 'hope':
            self.embedding = HOPE(d=self.dimension, beta=self.beta)
        elif self.model == 'sdne':
            self.embedding = SDNE(d=self.dimension, beta=self.beta, alpha=self.sdne_alpha, nu1=self.sdne_nu1,
                             nu2=self.sdne_nu2, K=self.sdne_K, n_units=self.sdne_n_units, n_iter=self.sdne_n_iter,
                             xeta=self.sdne_xeta, n_batch=self.sdne_n_batch)
        elif self.model == 'walklet':
            self.embedding = WalkletMachine(graph=G, dimensions=self.dimension, window_size=self.window_size,
                                       min_count=self.min_count, workers=self.workers, walk_type=self.walklet_type,
                                       walk_length=self.walk_len, walk_number=self.num_walks, walk_p=self.ret_p,
                                       walk_q=self.inout_q )
        elif self.model == 'prune':
            self.embedding = PRUNE(scope_name='default', dimension=self.dimension, lamb=self.prune_lamb,
                              learning_rate=self.prune_learning_rate, epoch=self.prune_epoch,
                              gpu_fraction=self.prune_gpu_fraction,
                              batchsize=self.prune_batch_size,
                              print_every_epoch=self.prune_print_every_epoch,
                              save_checkpoints=self.prune_save_checkpoints)

        Y, t = self.embedding.learn_embedding(graph=G, edge_f=None, is_weighted=False, no_python=True)

        file_name = self.file_name+'_'+self.model+'_'+str(self.id)
        embed_param = vars(self.embedding).copy()
        embed_param['_X'] = []
        embed_param['model'] = self.model
        embed_param['embed_dim'] = str(Y.shape)
        # embed_param.pop('X',None)
        vis_embd = VisualEmbedding(Path('../output/plot/net_embed/'+self.model+'/'+file_name), self.embedding.get_embedding(), di_graph=G, dim_reduce="pca",fig_labels=embed_param)
        vis_embd.expVis()

        return sparse.csr_matrix(self.embedding.get_embedding())[:X.shape[0]]

    def write_to_file(self,embed_file_name,net_embd_param_indx):
        embed_params = self.embedding.get_params()
        embed_data = sparse.csr_matrix(self.embedding.get_embedding())
        write_dict_to_csv('output/net_embed_model', embed_file_name, embed_params)
        data_write_pickle_file(embed_data, 'output/net_embed_model', embed_file_name + '_' + str(net_embd_param_indx),
                               'p')
