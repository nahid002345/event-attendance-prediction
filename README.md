# event-attendance-prediction

required library
* scipy
* numpy
* torch
> Pytorch import error _c* https://www.bountysource.com/issues/53478523-from-torch-_c-import-importerror-dll-load-failed-the-specified-module-could-not-be-found
* scrapy
* selenium
* xvfb
* urlparser
* python-twitter (pip install python-twitter)

##  cd crawl/scrapsched

##  scrapy crawl spider_name

## Breakdowns
### Tweets collection & analysis
* Sentiment Analysis of every tweets
* Tweets readability score (To detect language & relevent words in tweets)

## Issues
###environment setup
* node2vec2
``Exception: ./node2vec not found. Please compile snap, place node2vec in the path and grant executable permission ``

## TODO:
### Text cleaning
* Remove URL links
* Delete other language tweets
* Remove irrelevant words

## Feature Matrix
* Graph embedding (HARP, node2vec, walklet,PRUNE)
* concat multiple feature matrices
* Dimensionality reduction (PCA, t-sne)

## Classifier model and pipeline
* SVM
* Random Forest
* GBDT(Gradient boosting decision tree)
* FNN (Feed-forward neural network)
* RNN (Recurrent neural network)